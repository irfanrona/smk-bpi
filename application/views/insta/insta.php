

    <!-- Start Content -->

	<div id="content">

		<div class="container">

            <div class="page-content">

                <div class="row">

                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-8">

                        <p class="text-uppercase" style="margin-top:10px">Instagram Post</p>
                        <?= $this->session->flashdata('pesan') ?>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 text-right">

                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">

                            <i class="fa fa-plus"></i> Buat Postingan Baru

                        </button>

                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="collapse" id="collapseExample">

                            <div class="well">

                                <form method="POST" action="<?= base_url('api/createInsta') ?>">

                                    <div class="form-group">

                                        <label>Link Tautan <br> <!-- <small class="text-warning">format penulisannya adalah url_instagram / p / id_postingan</small> --></label>

                                        <input type="text" name="link" class="form-control" placeholder="ex : https://www.instagram.com/p/COi2PP2D9dl">

                                    </div>

                                    <div class="form-group">

                                        <label>Password</label>

                                        <input type="text" name="password" class="form-control" placeholder="password . . .">

                                    </div>

                                    <div class="form-group text-right">

                                        <button type="submit" class="btn btn-success">SIMPAN</button>

                                    </div>

                                </form>

                            </div>

                        </div>

                    </div>

                </div>

                <hr>

                <div class="flash-data" data-flashdata="<?=$this->session->flashdata('notif')?>"></div>

                <div class="row">

                    <div class="table-responsive">

                        <table class="table table-hover" id="mydata">

                            <thead>

                                <tr>

                                    <th class="text-center">NO</th>

                                    <th>Link</th>

                                    <th>Tanggal</th>

                                    <th class="text-center"><span class="fa fa-cog"></span></th>

                                </tr>

                            </thead>

                            <tbody>

                                <?php

                                    $no = 1;

                                    foreach ($instas as $data) :
                                        ?>

                                        <tr>
                                            <td class="text-center"><?= $no; ?></td>
                                            <td><a href="<?= $data->link; ?>"><?= $data->link; ?></a></td>
                                            <td><?= date('Y-m-d', strtotime($data->created_at)); ?></td>
                                            <td colspan="2" class="text-center">
                                                <a href="javascript:void();" data-id="<?= $data->id; ?>" data-link="<?= $data->link; ?>" data-created_at="<?= date('Y-m-d', strtotime($data->created_at)); ?>" data-toggle="modal" data-target="#form-edit" class="btn btn-warning"><span class="fa fa-pencil"></span></a>
                                                <a href="<?= base_url('api/deleteInsta/'.$data->id) ?>" data-id="<?= $data->id; ?>" class="btn btn-danger btn-delete"><span class="fa fa-trash"></span></a>
                                            </td>
                                        </tr>

                                    <?php
                                        $no++;
                                    endforeach;
                                    ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
		</div>
	</div>

    <!-- End Content -->

    <!-- ====================================== MODAL =============================================== -->

    <div class="modal fade" id="form-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top:100px">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Instagram Post</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
                <div class="modal-body">
                    <form method="POST" action="<?= base_url('api/updateInsta') ?>">
                        <input type="hidden" id="id" name="id">
                        <div class="form-group">
                            <label>Link</label>
                            <input type="text" class="form-control" id="link" name="link">
                        </div>
                        <div class="form-group">
                            <label>Tanggal Posting</label>
                            <input type="text" class="form-control date" id="created_at" name="created_at">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Ubah</button>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <script>

        $('#form-edit').on('show.bs.modal', function(event) {

            var div = $(event.relatedTarget)

            var modal = $(this)

            modal.find('#id').attr("value", div.data('id'));

            modal.find('#link').attr("value", div.data('link'));

            modal.find('#created_at').attr("value", div.data('created_at'));

        });

    </script>
    <script async src="//www.instagram.com/embed.js"></script>