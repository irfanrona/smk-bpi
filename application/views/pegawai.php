<!doctype html>

			

	<!-- Start Content -->

	<div id="content">

		<div class="container">

		<div class="page-content">

			<div class="row">

				<div class="col-md-7">

					<!-- Kepegawaian Heading -->

					<h4 class="classic-title"><span><i class="fa fa-users"></i> Sistem Kepegawaian SMK BPI Bandung</span></h4>

					<p style="margin-left:30px;">

					<strong style="font-size: 17px">Kepala Sekolah</strong><br>

						Doni Agus Maulana, S.Pd.

					</p>

					<p style="margin-left:30px">

					<strong style="font-size: 17px">Wakasek. Bidang Kurikulum</strong><br>

					Yayan Himawan, S.Pd. M.MPd.

					</p>

					<p style="margin-left:30px">

					<strong style="font-size: 17px">Wakasek. Bidang Kesiswaan</strong><br>

					Ade Aso, S.Pd.

					</p>

					<p style="margin-left:30px">

					<strong style="font-size: 17px">Wakasek. Bidang Hubungan Industri</strong><br>

					Dra. Hj. Anggani

					</p>

					<p style="margin-left:30px">

					<strong style="font-size: 17px">Ketua Tim Penjamin Mutu</strong><br>

					Irfan Rona, S.Pd.

					</p>

					<p style="margin-left:30px">

					<strong style="font-size: 17px">Staff Wakasek Bid. Kurikulum</strong><br>

					Uga Nugraha, S.Pd.

					</p>

					<p style="margin-left:30px">

					<strong style="font-size: 17px">Staff Wakasek Bid. Kesiswaan (Kreasi Seni)</strong><br>

					Asep Ediyana, S.Si.

					</p>

					<p style="margin-left:30px">

					<strong style="font-size: 17px">Staff Wakasek Bid. Kesiswaan (Pembinaan Ahlak)</strong><br>

					Agus Salim, M.Pd.

					</p>

					<p style="margin-left:30px">

					<strong style="font-size: 17px">Staff Wakasek Bid. Hubin</strong><br>

					Wahyu Sinarningsih, S.Pd.

					</p>

					<p style="margin-left:30px">

					<strong style="font-size: 17px">Staff Wakasek Bid. Hubin (BKK)</strong><br>

					Agista Akbari Wulandari, S.S.

					</p>

					<p style="margin-left:30px">

					<strong style="font-size: 17px">Pembina OSIS</strong><br>

					Andry Refianto

					</p>

					<p style="margin-left:30px">

					<strong style="font-size: 17px">Koordinator BK</strong><br>

					Ika Nurhayati, S.Psi.

					</p>

					<p style="margin-left:30px">

					<strong style="font-size: 17px">Kepala Program Kejuruan TKJ</strong><br>

					Acep Komarudin, S.Si.

					</p>

					<p style="margin-left:30px">

					<strong style="font-size: 17px">Kepala Program Kejuruan OTKP</strong><br>

					Fani Setiani, S.Pd.

					</p>

					<p style="margin-left:30px">

					<strong style="font-size: 17px">Kepala Program Kejuruan RPL</strong><br>

					M. Noor Basuki, S.Si.

					</p>

					<p style="margin-left:30px">

					<strong style="font-size: 17px">Guru Produktif RPL</strong><br>

						Mitha Hermina, S.Pd. 
						<strong> & </strong>
						Irfan Rona, S.Pd.

					</p>

					<p style="margin-left:30px">

					<strong style="font-size: 17px">Guru Produktif TKJ</strong><br>

						Galih Nalapraya, S.Si.
						<strong> & </strong>
						Uga Nugraha Syafari, S.Pd.

					</p>

					<p style="margin-left:30px">

					<strong style="font-size: 17px">Guru Produktif OTKP</strong><br>

						Fani Setiani, S.Pd.
						<strong> & </strong>
						Ignur Oktaviani, S.Pd.

					</p>
                    
                    
					<p style="margin-left:30px">

					<strong style="font-size: 17px">Guru Mata Pelajaran</strong>
					
					<table style="margin-left:30px; font-size:15px;">
					    
					    <tr height="20px">
        					<td width="220px">- Agus Salim, M.Pd.I</td>
        					<td width="20px">: </td>
        					<td style="color:#000080"> Pendidikan Agama Islam</td>
        			    </tr>
        			    
        				<tr height="20px">
        					<td width="220px">- Agus Nugroho, S.Pd.</td>
        					<td width="20px">: </td>
        					<td style="color:#000080"> Sejarah Indonesia</td>
        			    </tr>
        			    
        				<tr height="20px">
        					<td width="220px">- Andry Refianto</td>
        					<td width="20px">: </td>
        					<td style="color:#000080"> Pendidikan Kewarganegaraan</td>
        			    </tr>
        			    
        			    <tr height="20px">
        					<td width="220px">- Wahyu Sinarningsih, S.Pd.</td>
        					<td width="20px">: </td>
        					<td style="color:#000080"> Bahasa Indonesia</td>
        			    </tr>
        			    
        			    <tr height="20px">
        					<td width="220px">- Vera Aldhilah, S.Pd.</td>
        					<td width="20px">: </td>
        					<td style="color:#000080"> Bahasa Indonesia</td>
        			    </tr>
        			    
        			    <tr height="20px">
        					<td width="220px">- Agista Akbari Wulandari, S.S.</td>
        					<td width="20px">: </td>
        					<td style="color:#000080"> Bahasa Inggris</td>
        			    </tr>
        			    
        			    <tr height="20px">
        					<td width="220px">- Novarida Maisha, S.Pd.</td>
        					<td width="20px">: </td>
        					<td style="color:#000080"> Bahasa Inggris</td>
        			    </tr>
        			    
        			    <tr height="20px">
        					<td width="220px">- Taufan Hikhaiat, S.Pd. </td>
        					<td width="20px">: </td>
        					<td style="color:#000080"> Bahasa Sunda </td>
        			    </tr>
        			    
        			    <tr height="20px">
        					<td width="220px">- Resti Khairun Nissa, S.Pd. </td>
        					<td width="20px">: </td>
        					<td style="color:#000080"> Matematika </td>
        			    </tr>
        			    
        			    <tr height="20px">
        					<td width="220px">- Inri Rahmawati, M.Pd. </td>
        					<td width="20px">: </td>
        					<td style="color:#000080"> Matematika </td>
        			    </tr>
        			    
        			    <tr height="20px">
        					<td width="220px">- Kystri Yanuar Putri, S.Si.</td>
        					<td width="20px">: </td>
        					<td style="color:#000080"> Kimia/ IPA </td>
        			    </tr>
        			    
        			    <tr height="20px">
        					<td width="220px">- Asep Ediyana Sulaksana, S.Sn. </td>
        					<td width="20px">: </td>
        					<td style="color:#000080"> Seni Budaya</td>
        			    </tr>
        			    
        			    <tr height="20px">
        					<td width="220px">- Luby Tsani Ahwady, S.Si. </td>
        					<td width="20px">: </td>
        					<td style="color:#000080"> Pendidikan Jasmani </td>
        			    </tr>

						<tr height="20px">
        					<td width="220px">- Rifki Rahmat Hidayatulloh, S.Psi. </td>
        					<td width="20px">: </td>
        					<td style="color:#000080"> Bimbingan dan Konseling </td>
        			    </tr>
        			    
        			</table>

					</p>
					
					<p style="margin-left:30px">
					
					<strong style="font-size: 17px">Tenaga Administrasi Sekolah</strong><br>

						- N. Dedeh Sumiati <br>

						- Kristina Hasanah, A.Md. <br>

						- Wawan Juansyah <br>

						- Yudi Satria <br>
					</p>

				</div>



				<div class="col-md-5" style="margin-top:37px;">

					<div class="item"><img alt="" src="<?php echo base_url();?>assets/images/staff1.png"></div>


				</div>
				
				<div class="col-md-5" style="margin-top:50px;">

					<div class="item"><img alt="" src="<?php echo base_url();?>assets/images/staff2.png"></div>

				</div>
				
				<div class="col-md-5" style="margin-top:50px;">

					<div class="item"><img alt="" src="<?php echo base_url();?>assets/images/staff3.png"></div>

				</div>
				
				<!--
				<div class="col-md-5" style="margin-top:100px;">

					<!-- Start Touch Slider

					<div class="touch-slider" data-slider-navigation="true" data-slider-pagination="true">

					<div class="item"><img alt="" src="<?php echo base_url();?>assets/images/smk1.jpg"></div>

					<div class="item"><img alt="" src="<?php echo base_url();?>assets/images/smk2.jpg"></div>

					<div class="item"><img alt="" src="<?php echo base_url();?>assets/images/smk3.jpg"></div>

					<div class="item"><img alt="" src="<?php echo base_url();?>assets/images/smk4.jpg"></div>

					<div class="item"><img alt="" src="<?php echo base_url();?>assets/images/smk5.jpg"></div>

					<div class="item"><img alt="" src="<?php echo base_url();?>assets/images/smk6.jpg"></div>

					<div class="item"><img alt="" src="<?php echo base_url();?>assets/images/smk7.jpg"></div>

					</div>

					<!-- End Touch Slider

				</div>-->
				

			</div>

		</div>

		</div>

	</div>

	<!-- End Content -->