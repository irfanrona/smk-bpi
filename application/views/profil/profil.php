<!doctype html>
			
	<!-- Start Content -->
	<div id="content">
		<div class="container">
		<div class="page-content">
			<div class="row">
				<div class="col-md-7">
					<!-- Profil SMK Heading -->
					<h4 class="classic-title"><span><i class="fa fa-institution"></i> Profil Umum SMK BPI Bandung</span></h4>
					<!-- Tentang SMK -->
					<p style="text-align:justify"><img style="float:left; width:290px; margin-right:30px;" alt="" src="<?php echo base_url();?>assets/images/logofix.png" />Teknologi Informasi dan Komputer telah menjadi bagian dalam kehidupan masyarakat di era globalisasi dan pasar bebas. Masyarakat saat ini tidak lagi bisa menghindar dari zaman digital. Sebagai jembatan menuju penguasaan teknologi Informasi dan Komunikasi. Yang menjadikan SMK BPI Bandung terbentuk.</p>
					<p style="text-align:justify">Sekolah Menegah Kejuruan BPI Bandung adalah salah satu Sekolah berbasis Teknologi yang mempunyai 3 Program Studi Unggulan Ter-Akreditasi A. Sekolah yang mengedepankan Pendidikan dan Akhlak Mulia Berbasis Penilaian Holistik, menjadikan peserta didik SMK BPI Bandung Bermartabat, Berkualitas, dan Terpercaya.</p>
					<p style="text-align:justify">Sekolah yang mempunyai letak strategis pada pusat Kota Bandung, yang menjadikan mobilitas Sekolah Menengah Kejuruan BPI Bandung menjadi mudah diakses. Dan Di lengkapi pula dengan pendidik yang berkompeten dan ahli pada setiap Program Studinya.</p>
					<p style="text-align:justify">Program Studi pada SMK BPI Bandung antara lain :<br/>
						<div class="col-md-6">
							<ul class="icons-list" >
							<li><i class="fa fa-check-circle"></i> Otomatisasi dan Tata Kelola Perkantoran.</li>
							<li><i class="fa fa-check-circle"></i> Rekayasa Perangkat Lunak.</li>
							<li><i class="fa fa-check-circle"></i> Teknik Komputer Jaringan.</li>
							</ul>
						</div>
					</p>
				</div>

				<div class="col-md-5" style="margin-top:37px;">
					<!-- Start Touch Slider -->
					<div class="touch-slider" data-slider-navigation="true" data-slider-pagination="true">
					<div class="item"><img alt="" src="<?php echo base_url();?>assets/images/smk1.jpg"></div>
					<div class="item"><img alt="" src="<?php echo base_url();?>assets/images/smk2.jpg"></div>
					<div class="item"><img alt="" src="<?php echo base_url();?>assets/images/smk3.jpg"></div>
					<div class="item"><img alt="" src="<?php echo base_url();?>assets/images/smk4.jpg"></div>
					<div class="item"><img alt="" src="<?php echo base_url();?>assets/images/smk5.jpg"></div>
					<div class="item"><img alt="" src="<?php echo base_url();?>assets/images/smk6.jpg"></div>
					<div class="item"><img alt="" src="<?php echo base_url();?>assets/images/smk7.jpg"></div>
					</div>
					<!-- End Touch Slider -->
				</div>
			</div>
		</div>
		</div>
	</div>
	<!-- End Content -->