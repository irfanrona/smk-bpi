<!doctype html>
	

	<!-- Start Content -->

    <div id="content">

      <div class="container">

        <div class="page-content">





          <div class="row">



            <div class="col-md-7">



              <!-- Ekskul Heading -->

              <h4 class="classic-title"><span><i class="fa fa-child"></i> Ekstrakurikuler SMK BPI Bandung</span></h4>



              <!-- Tentang Ekskul -->

              <p style="text-align:justify; margin-right:35px;"><img style="float:left; width:190px; height:160px; margin-right:35px;" alt="" src="<?php echo base_url();?>assets/images/icon/undraw_soccer.svg" /> SMK BPI Bandung menyelenggarakan berbagai kegiatan ekstrakurikuler untuk memfasilitasi peserta didik agar dapat mengembangkan kemampuan, minat, dan bakat yang dimilikinya, baik di bidang kompetensi, olahraga, maupun kesenian. </p>

		<br><br><br><br><br>             


	<!-- Daftar Ekskul -->

          <h4 class="classic-title" style="margin-top:20px;"><span><i class="fa fa-calendar-check-o"></i> Daftar Ekstrakurikuler</span></h4>

				<p style="margin-left:30px;">

					<b>Terdapat beberapa ekstrakurikuler yang ada di SMK BPI Bandung, diantaranya adalah:</b>

					<div class="row" style="margin-left:30px;">

						<div class="col-md-6">

						  <ul class="icons-list" >

							<li><i class="fa fa-check-circle"></i> Akustik</li>

							<li><i class="fa fa-check-circle"></i> Animasi</li>

							<li><i class="fa fa-check-circle"></i> Futsal</li>

							<li><i class="fa fa-check-circle"></i> Basket</li>

							<li><i class="fa fa-check-circle"></i> Muaythai</li>
							
							<li><i class="fa fa-check-circle"></i> Mikrotik</li>

							<li><i class="fa fa-check-circle"></i> Modern Dance</li>
							
							<li><i class="fa fa-check-circle"></i> Pramuka</li>

							<li><i class="fa fa-check-circle"></i> Rohis</li>

							<li><i class="fa fa-check-circle"></i> Japanese Club</li>

							<li><i class="fa fa-check-circle"></i> English Club</li>

							<li><i class="fa fa-check-circle"></i> Math Club</li>

							<li><i class="fa fa-check-circle"></i> Panahan</li>

							<li><i class="fa fa-check-circle"></i> Softball</li>

						  </ul>

						</div>

					</div>

				</p>

          <!-- End Visi Misi Osis -->
   


	</div>



            <div class="col-md-5 col-sm-12 col-xs-12" style="margin-top:37px;">

            
             <div class="team-member">

                <div class="member-photo">

                  <img alt="" src="<?php echo base_url();?>assets/images/eskul/mikrotik.png" />

                  <div class="member-name">Ekskul Mikrotik <span>SMK BPI</span></div>

                </div>

              </div>
              
              <br><br>
            

              <div class="team-member">

                <div class="member-photo">

                  <img alt="" src="<?php echo base_url();?>assets/images/eskul/akustik1.png" />

                  <div class="member-name">Ekskul Akustik <span>SMK BPI</span></div>

                </div>

              </div>
              


            </div>

		</div>	

         

		</div>


		<!-- Start Recent Projects Carousel -->

					<div class="recent-projects" style="margin-top:40px;">

					  <h4 class="classic-title"><span>Dokumentasi Ekstrakurikuler SMK BPI Bandung</span></h4>

					  <div class="projects-carousel touch-carousel">


                    <!-- Foto Ekskul -->
                    
                        <div class="portfolio-item item">

						  <div class="portfolio-border">

							<div class="portfolio-thumb">

							  <a class="lightbox" data-lightbox-type="ajax" href="<?php echo base_url();?>assets/images/eskul/pramuka1.jpeg">

								<div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>

								<img alt="" src="<?php echo base_url();?>assets/images/eskul/pramuka1.jpeg" />

							  </a>

							</div>

							<div class="portfolio-details">

							  <a href="#">

								<h4> Pramuka </h4>

								<span>Ekskul SMK BPI</span>

							  </a>

							</div>

						  </div>

						</div>
						
						
                    
                    
                    
						<div class="portfolio-item item">

						  <div class="portfolio-border">

							<div class="portfolio-thumb">

							  <a class="lightbox" data-lightbox-type="ajax" href="<?php echo base_url();?>assets/images/eskul/futsal1.png">

								<div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>

								<img alt="" src="<?php echo base_url();?>assets/images/eskul/futsal1.png" />

							  </a>

							</div>

							<div class="portfolio-details">

							  <a href="#">

								<h4> Futsal </h4>

								<span>Ekskul SMK BPI</span>

							  </a>

							</div>

						  </div>

						</div>



						<div class="portfolio-item item">

						  <div class="portfolio-border">

							<div class="portfolio-thumb">

							  <a class="lightbox" title="" href="<?php echo base_url();?>assets/images/eskul/futsal2.png">

								<div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>

								<img alt="" src="<?php echo base_url();?>assets/images/eskul/futsal2.png" />

							  </a>

							</div>

							<div class="portfolio-details">

							  <a href="#">

								<h4> Futsal </h4>

								<span>Ekskul SMK BPI</span>

							  </a>

							</div>

						  </div>

						</div>


						<div class="portfolio-item item">

						  <div class="portfolio-border">

							<div class="portfolio-thumb">

							  <a class="lightbox" title="" href="<?php echo base_url();?>assets/eskul/muaythai2.png">

								<div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>

								<img alt="" src="<?php echo base_url();?>assets/images/eskul/muaythai2.png" />

							  </a>

							</div>

							<div class="portfolio-details">

							  <a href="#">

								<h4> Muaythai </h4>

								<span>Ekskul SMK BPI </span>

							  </a>

							</div>

						  </div>

						</div>
						
							<div class="portfolio-item item">

						  <div class="portfolio-border">

							<div class="portfolio-thumb">

							  <a class="lightbox" title="" href="<?php echo base_url();?>assets/eskul/muaythai2.png">

								<div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>

								<img alt="" src="<?php echo base_url();?>assets/images/eskul/muaythai1.png" />

							  </a>

							</div>

							<div class="portfolio-details">

							  <a href="#">

								<h4> Muaythai </h4>

								<span>Ekskul SMK BPI </span>

							  </a>

							</div>

						  </div>

						</div>

						<div class="portfolio-item item">

						  <div class="portfolio-border">

							<div class="portfolio-thumb">

							  <a class="lightbox" title="" href="<?php echo base_url();?>assets/images/eskul/moderndance1.png">

								<div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>

								<img alt="" src="<?php echo base_url();?>assets/images/eskul/moderndance1.png" />

							  </a>

							</div>

							<div class="portfolio-details">

							  <a href="#">

								<h4> Modern Dance </h4>

								<span>Ekskul SMK BPI </span>

							  </a>

							</div>

						  </div>

						</div>

						<div class="portfolio-item item">

						  <div class="portfolio-border">

							<div class="portfolio-thumb">

							  <a class="lightbox" title="" href="<?php echo base_url();?>assets/images/eskul/moderndance3.png">

								<div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>

								<img alt="" src="<?php echo base_url();?>assets/images/eskul/moderndance3.png" />

							  </a>

							</div>

							<div class="portfolio-details">

							  <a href="#">

								<h4> Modern Dance </h4>

								<span> Ekskul SMK BPI </span>

							  </a>

							</div>

						  </div>

						</div>
						
						
						<div class="portfolio-item item">

						  <div class="portfolio-border">

							<div class="portfolio-thumb">

							  <a class="lightbox" title="" href="<?php echo base_url();?>assets/images/eskul/akustik3.png">

								<div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>

								<img alt="" src="<?php echo base_url();?>assets/images/eskul/akustik3.png" />

							  </a>

							</div>

							<div class="portfolio-details">

							  <a href="#">

								<h4> Akustik </h4>

								<span> Ekskul SMK BPI </span>

							  </a>

							</div>

						  </div>

						</div>

						


					  </div>

					</div>

				</div>


	</div>

	</div>