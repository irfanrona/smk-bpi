<!doctype html>



	<!-- Start Content -->

    <div id="content">

      <div class="container">

        <div class="page-content">





          <div class="row">



            <div class="col-md-7">



              <!-- RPL Heading -->

              <h4 class="classic-title"><span><i class="fa fa-laptop"></i> Rekayasa Perangkat Lunak</span></h4>



              <!-- Tentang RPL -->

              <p style="text-align:justify"><img style="float:left; width:150px; margin-right:30px;" alt="" src="<?php echo base_url();?>assets/images/logo_rpl2.png" />Rekayasa Perangkat Lunak (RPL) merupakan salah satu program keahlian di SMK BPI, dimana pada program ini siswa belajar bagaimana cara membuat program komputer dan cara berpikir dalam menyelesaikan masalah. Keahlian ini mencetak siswa-siswi yang handal di bidang Aplikasi Mobile, Aplikasi Web dan Web Design yang merupakan profesi menjanjikan di masa depan</p>

			  <p style="text-align:justify">Kompetensi Keahlian RPL mempelajari dan mendalami  semua cara-cara pengembangan perangkat lunak termasuk pembuatan, pemeliharaan, manajemen organisasi pengembangan perangkat lunak dan manajemen kualitas. Adapun yang dipelajari pada Program Keahlian RPL yaitu mulai dari pembuatan website, aplikasi, game dan semua yang berkaitan dengan pemrograman. Bukan hanya itu, pada program ini juga dapat dipelajari materi yang berkaitan di bidang desain, video dan fotografi.</p>


            </div>

            

            <div class="col-md-5" style="margin-top:37px;">

                

              <!-- Start Touch Slider -->
              
    
                <!--  <div class="touch-slider" data-slider-navigation="true" data-slider-pagination="true"> -->
                  
                <div>  

                <div class="item"><img alt="" src="<?php echo base_url();?>assets/images/RPL.jpg"></div>

            <!--  
                <div class="item"><img alt="" src="<?php echo base_url();?>assets/images/tkj2.jpg"></div>

                <div class="item"><img alt="" src="<?php echo base_url();?>assets/images/tkj3.jpg"></div>
            -->

              </div>
            

              <!-- End Touch Slider -->



            </div>

			

          </div>



          <!-- Divider -->

          <div class="hr1" style="margin-bottom:50px;"></div>



          <div class="row">



            <div class="col-md-6">



              <!-- Classic Heading -->

              <h4 class="classic-title"><span>Kompetensi Keahlian RPL</span></h4>



              <div class="skill-shortcode">

					<p><b>Kompetensi keahlian RPL SMK BPI sebagai Jurusan yang bergerak di bidang IT dan Programming.</b> Kompetensi Keahlian dan Materi Pembelajaran menyesuaikan dengan teknologi diantaranya adalah:

					<div class="row">

						<div class="col-md-8">

						  <ul class="icons-list" style="margin-left:30px;">

							<li><i class="fa fa-check-circle"></i> Teknik pemrograman komputer. </li>

							<li><i class="fa fa-check-circle"></i> Perancangan perangkat lunak komputer. </li>
							
							<li><i class="fa fa-check-circle"></i> Perancangan basis data komputer. </li>
							
							<li><i class="fa fa-check-circle"></i> Pemrograman Aplikasi mobile. </li>
							
							<li><i class="fa fa-check-circle"></i> Pemrograman Website. </li>
							
							<li><i class="fa fa-check-circle"></i> Produk kreatif digital (Startup).</li>

						  </ul>

						</div>

					</div>

				  </p>
                
				  <p><b>Ruang Lingkup Kerja</b> berbagai pekerjaan yang dapat dipilih oleh lulusan RPL:

					<div class="row">

						<div class="col-md-9">

						  <ul class="icons-list" style="margin-left:30px;">

							<li><i class="fa fa-check-circle"></i> Web Application Programmer </li>

							<li><i class="fa fa-check-circle"></i> Mobile Application Programmer (Java and Android) </li>
							
							<li><i class="fa fa-check-circle"></i> Database Programmer </li>
							
							<li><i class="fa fa-check-circle"></i> Interfacing Programmer </li>
							
							<li><i class="fa fa-check-circle"></i> Desktop Application Programmer </li>
							
							<li><i class="fa fa-check-circle"></i> Game Programmer </li>
							
							<li><i class="fa fa-check-circle"></i> Hardware and Software Technicians </li>
							
							<li><i class="fa fa-check-circle"></i> IT Support and IT Staff </li>
							
							<li><i class="fa fa-check-circle"></i> Pekerjaan di bidang IT lainnya </li>

						  </ul>

						</div>

					</div>

				  </p>
				

				  <p><b>" Dari segi peluang kerja setelah lulus sangat banyak peluangnya. Mulai dari menjadi programmer, designer, membuka startup sendiri dan melanjutkan ke Perguruan Tinggi ."</b></p>

              </div>

            </div>



            <div class="col-md-6">



              <!-- Classic Heading -->

              <h4 class="classic-title"><span>Fasilitas RPL SMK BPI</span></h4>



              <!-- Accordion -->

              <div class="panel-group" id="accordion">



                <!-- Start Accordion 1 -->

                <div class="panel panel-default">

                  <!-- Toggle Heading -->

                  <div class="panel-heading">

                    <h4 class="panel-title">

                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse-1">

                        <i class="fa fa-angle-up control-icon"></i>

                        <i class="fa fa-desktop"></i> Fasilitas RPL

                      </a>

                    </h4>

                  </div>

                  <!-- Toggle Content -->

                  <div id="collapse-1" class="panel-collapse collapse in">

                    <div class="panel-body"><img class="img-thumbnail image-text" style="float:left; width:150px;" alt="" src="<?php echo base_url();?>assets/images/lab_rpl.JPG" />
                    
						<p>Adapun <b>Fasilitas</b> yang ada di Jurusan Rekayasa Perangkat Lunak adalah :
    
							<div class="row">

								<div class="col-md-8">

								  <ul class="icons-list" >

									<li><i class="fa fa-check-circle"></i> Laboratorium Komputer RPL.</li>
									
									<li><i class="fa fa-check-circle"></i> Akses Internet cepat.</li>

        							<li><i class="fa fa-check-circle"></i> Hotspot Area di seluruh wilayah sekolah.</li>
        							
        							<li><i class="fa fa-check-circle"></i> Hardware dan Software pendukung.</li>

									<li><i class="fa fa-check-circle"></i> Peralatan Pembelajaran Lengkap.</li>

								  </ul>

								</div>

							</div>

						</p>

					</div>

                  </div>

                </div>

                <!-- End Accordion 1 -->



              </div>

            </div>



          </div>



          <!-- Divider -->

          <div class="hr1" style="margin-bottom:50px;"></div>



          <!-- Classic Heading -->

          <h4 class="classic-title"><span>Keunggulan Kompetensi Keahlian RPL SMK BPI</span></h4>



          <!-- Keunggulan -->

				<p>

					<b>Dari segi pendidikan</b>

					<div class="row">

						<div class="col-md-6">

						  <ul class="icons-list" >

							<li><i class="fa fa-check-circle"></i> Pembelajaran mudah dipahami.</li>

							<li><i class="fa fa-check-circle"></i> Masih berstatus siswa sudah bisa menghasilkan uang (proyek situs web, desain, dll) .</li>

							<li><i class="fa fa-check-circle"></i> Dapat memperdalam jurusan lain yang serumpun (seperti Animasi, Multimedia, Desain grafis, Pemrograman).</li>

							<li><i class="fa fa-check-circle"></i> Menjadi jurusan yang memperdalam ilmu Teknologi, baik Software ataupun Hardware.</li>

							<li><i class="fa fa-check-circle"></i> Bekerjasama dengan beberapa Perusahaan IT ternama.</li>

							<li><i class="fa fa-check-circle"></i> Lulusan Selain dapat Ijazah juga dapat Sertifikat yang diakui </li>

						  </ul>

						</div>

					</div>

				</p>

				<p>

					<b>Dari segi pekerjaan</b>

					<div class="row">

						<div class="col-md-6">

						  <ul class="icons-list" >

							<li><i class="fa fa-check-circle"></i> Dapat menjadi seorang ahli IT di bidang Programming.</li>

							<li><i class="fa fa-check-circle"></i> Dapat membuka usaha startup.</li>
							
							<li><i class="fa fa-check-circle"></i> Dapat membuat atau mengembangkan aplikasi sendiri.</li>

							<li><i class="fa fa-check-circle"></i> Dapat melanjutkan kuliah dengan jurusan serumpun.</li>

							<li><i class="fa fa-check-circle"></i> Peluang kerja sangat luas dengan gaji menjanjikan.</li>

						  </ul>

						</div>

					</div>

				</p>
				
				

				

          <!-- End Keunggulan -->

		  

		  <!-- Divider -->

          <div class="hr1" style="margin-bottom:50px;"></div>



          <!-- Classic Heading

          <h4 class="classic-title"><span>Prestasi RPL SMK BPI</span></h4>

		  

		  <!-- Divider

          <div class="hr1"></div>

		  

		  <table border="1" style="text-align:center;margin-top:10px;">

			<tr style="background-color:#000080;color:#f8ba01">

				<th width="40px" height="30px;" style="text-align:center;">No.</th>

				<th width="290px" style="text-align:center;">Prestasi</th>

				<th width="200px" style="text-align:center;">Penyelenggara</th>
				
				<th width="150px" style="text-align:center;">Cakupan Lomba</th>

				<th width="70px" style="text-align:center;">Tahun</th>

			</tr>

			<tr>

				<td>1.</td>

				<td>Juara 2 lomba Web Development</td>

				<td>Ilmu Komputer UPI Bandung</td>
				
				<td>Tingkat Provinsi</td>

				<td>2019</td>

			</tr>

			<tr>

				<td>2.</td>

				<td>Juara 2 lomba Web Desain</td>

				<td>STMIK AMIK Bandung</td>
				
				<td>Tingkat Kota</td>

				<td>2018</td>

			</tr>

		  </table>

		    -->

          <!-- Start Recent Projects Carousel -->
          <!--

        <div class="recent-projects" style="margin-top:40px;">

          <h4 class="classic-title"><span>Tentang TKJ SMK BPI</span></h4>

          <div class="projects-carousel touch-carousel">



            <div class="portfolio-item item">

              <div class="portfolio-border">

                <div class="portfolio-thumb">

                  <a class="lightbox" data-lightbox-type="ajax" href="<?php echo base_url();?>assets/images/portfolio-1/tkj1_open.jpg">

                    <div class="thumb-overlay"><i class="fa fa-play"></i></div>

                    <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/tkj1.jpg" />

                  </a>

                </div>

                <div class="portfolio-details">

                  <a href="#">

                    <h4>Teknik Komputer Jaringan</h4>

                    <span>by: TIM ICT SMK BPI Bandung</span>

                  </a>

                </div>

              </div>

            </div>



            <div class="portfolio-item item">

              <div class="portfolio-border">

                <div class="portfolio-thumb">

                  <a class="lightbox" title="Profil TKJ SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/tkj2_open.jpg">

                    <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>

                    <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/tkj2.jpg" />

                  </a>

                </div>

                <div class="portfolio-details">

                  <a href="#">

                    <h4>Hasil Karya Anak TKJ</h4>

                    <span>By : TIM ICT SMK BPI Bandung</span>

                  </a>

                </div>

              </div>

            </div>



            <div class="portfolio-item item">

              <div class="portfolio-border">

                <div class="portfolio-thumb">

                  <a class="lightbox" title="Profil TKJ SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/tkj3_open.jpg">

                    <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>

                    <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/tkj3.jpg" />

                  </a>

                </div>

                <div class="portfolio-details">

                  <a href="#">

                    <h4>Hasil Karya Anak TKJ</h4>

                    <span>By : TIM ICT SMK BPI Bandung</span>

                  </a>

                </div>

              </div>

            </div>

			

			<div class="portfolio-item item">

              <div class="portfolio-border">

                <div class="portfolio-thumb">

                  <a class="lightbox" title="Profil TKJ SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/tkj4_open.jpg">

                    <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>

                    <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/tkj4.jpg" />

                  </a>

                </div>

                <div class="portfolio-details">

                  <a href="#">

                    <h4>Hasil Karya Anak TKJ</h4>

                    <span>By : TIM ICT SMK BPI Bandung</span>

                  </a>

                </div>

              </div>

            </div>

			

			<div class="portfolio-item item">

              <div class="portfolio-border">

                <div class="portfolio-thumb">

                  <a class="lightbox" title="Profil TKJ SMK BPI" href="<?php echo base_url();?>assets/images/portfolio-1/tkj5_open.jpg">

                    <div class="thumb-overlay"><i class="fa fa-arrows-alt"></i></div>

                    <img alt="" src="<?php echo base_url();?>assets/images/portfolio-1/tkj5.jpg" />

                  </a>

                </div>
                

                <div class="portfolio-details">

                  <a href="#">

                    <h4>Hasil Karya Anak TKJ</h4>

                    <span>Sistem Operasi</span>

                    <span>By : TIM ICT SMK BPI Bandung</span>

                  </a>

                </div>

              </div>

            </div>
            -->
			

          </div>

        </div>

	</div>

  </div>

</div>