<style type="text/css">
	@media only screen and (max-width : 550px) {

		.pricing-tables {
			margin-left: -2px;
		}
	}

	@media(min-width : 375px) {

		.pricing-tables {
			margin-left: -30px;
		}
	}

	@media(min-width : 414px) {

		.pricing-tables {
			margin-left: -20px;
		}
	}

	@media(min-width : 768px) {
		.pricing-tables {
			margin-left: 150px;
		}

		.pricing-tables .highlight-plan {
			width: 400px;
		}
	}

	@media (min-width: 1024px) {
		.pricing-tables {
			margin-left: 350px;
		}

		.pricing-tables .highlight-plan {
			width: 400px;
		}

	}

	@media (min-width: 1152px) {
		.pricing-tables {
			margin-left: 345px;
		}

		.pricing-tables .highlight-plan {
			width: 400px;
		}

	}
</style>

<section id="home">
	<!-- Tampilan Carousel -->
	<div id="main-slide" class="carousel slide" data-ride="carousel">

		<ol class="carousel-indicators">
			<li data-target="#main-slide" data-slide-to="0" class="active"></li>
			<li data-target="#main-slide" data-slide-to="1"></li>
			<li data-target="#main-slide" data-slide-to="2"></li>
			<li data-target="#main-slide" data-slide-to="3"></li>
			<li data-target="#main-slide" data-slide-to="4"></li>
			<li data-target="#main-slide" data-slide-to="5"></li>
		</ol>
		<div class="carousel-inner">
			<div class="item active">
				<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/first.jpg" alt="slider">
				<div class="slider-content">
					<div class="col-md-12 text-center">
						<h2 class="animated2">
							<span style="color:#000080"><b>Welcome to <strong>SMK BPI</strong> Site</b></span>
						</h2>
						<h3 class="animated3">
							<span style="color:#000080"><b>Bermartabat, Berkualitas, dan Terpercaya !</b></span>
						</h3>
						</p>
					</div>
				</div>
			</div>

			<!-- <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/maulid.png" alt="slider">
						<div class="slider-content">
							<div class="col-md-12 text-center">
							</div>
						</div>
				  </div>				  				   -->
			<!-- <div class="item">
				<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/ppdbnew.png" alt="slider">
				<div class="slider-content">
					<div class="col-md-12 text-center">
						<p class="animated6" style="margin-top:60px;margin-left:130px;"><a href="https://api.whatsapp.com/send?phone=6283821081572&text=Saya%20tertarik%20untuk%20mendaftarkan%20anak%20saya%20di%20SMK%20BPI" class="slider btn btn-system btn-large">Daftar Sekarang!</a>
						</p>
					</div>
				</div>
			</div> -->
			<!-- <div class="item">
				<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/yayasan1.png" alt="slider">
				<div class="slider-content">
					<div class="col-md-5">
						<p class="animated6" style="margin-top:60px;"><a href="https://ppdb.bpi.web.id/" class="slider btn btn-system btn-large">Web Yayasan BPI</a>
						</p>
					</div>
				</div>
			</div> -->
			<!-- <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/jurusan.jpg" alt="slider">
						<div class="slider-content">
							<div class="col-md-12 text-center">
							</div>
						</div>
				  </div> -->
			<div class="item">
				<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/ap.jpg" alt="slider">
				<div class="slider-content">
					<div class="col-md-12 text-center">
					</div>
				</div>
			</div>
			<div class="item">
				<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/rpl.jpg" alt="slider">
				<div class="slider-content">
					<div class="col-md-12 text-center">
					</div>
				</div>
			</div>
			<div class="item">
				<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/tkj.jpg" alt="slider">
				<div class="slider-content">
					<div class="col-md-12 text-center">
					</div>
				</div>
			</div>
			<!-- <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/pengawas.jpg" alt="slider">
					<div class="slider-content">
					  <div class="col-md-12 text-center">
						<p class="animated6" style="margin-top:70px;"><a href="http://psmk.kemdikbud.go.id/" class="slider btn btn-system btn-large">Pengawas SMK</a>
						</p>
					  </div>
					</div>
				  </div> -->
			<!-- <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/mou.jpg" alt="slider">
						<div class="slider-content">
							<div class="col-md-12 text-center">
							</div>
						</div>
				  </div>
				  <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/pahlawan.jpg" alt="slider">
						<div class="slider-content">
							<div class="col-md-12 text-center">
							</div>
						</div>
				  </div> -->
			<!-- <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/baktis.jpg" alt="slider">
					<div class="slider-content">
					  <div class="col-md-12 text-center">
						<p class="animated6" style="margin-top:70px;"><a href="http://jabar.tribunnews.com/2017/04/21/peringati-hari-kartini-pelajar-smk-bpi-gelar-bakti-sosial-di-pasar-palasari" class="slider btn btn-system btn-large">Tribun Jabar</a>
						</p>
					  </div>
					</div>
				  </div> -->
			<!-- <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/ujikom-rpl.jpg" alt="slider">
						<div class="slider-content">
							<div class="col-md-12 text-center">
								
							</div>
						</div>
				  </div>
				  <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/ujikom-tkj.jpg" alt="slider">
						<div class="slider-content">
							<div class="col-md-12 text-center">
								
							</div>
						</div>
				  </div> -->
			<!-- <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/verifikasi.jpg" alt="slider">
						<div class="slider-content">
							<div class="col-md-12 text-center">
								
							</div>
						</div>
				  </div> -->
			<!-- <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/table.jpg" alt="slider">
						<div class="slider-content">
							<div class="col-md-12 text-center">
								
							</div>
						</div>
				  </div>
				  <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/cantik.jpg" alt="slider">
						<div class="slider-content">
							<div class="col-md-12 text-center">
							</div>
						</div>
				  </div> -->
			<!-- <div class="item">
					<img class="img-responsive" src="<?php echo base_url(); ?>assets/images/slider/kelompok_rohis.jpg" alt="slider">
						<div class="slider-content">
							<div class="col-md-12 text-center">
								
							</div>
						</div>
				  </div> -->

		</div>

		<!-- Tampilan Panah -->
		<a class="left carousel-control" href="#main-slide" data-slide="prev">
			<span><i class="fa fa-angle-left"></i></span>
		</a>
		<a class="right carousel-control" href="#main-slide" data-slide="next">
			<span><i class="fa fa-angle-right"></i></span>
		</a>
	</div>
</section>

<div id="content" class="full-sections">
	<div class="section" style="padding-top:60px; padding-bottom:60px; border-top:1px solid #eee; border-bottom:1px solid #eee; background:#f9f9f9;">
		<div class="container">

			<div class="row">

				<div class="col-md-6">

					<div class="big-title">
						<h1><strong>Mengapa Harus</strong> memilih SMK ?</h1>
					</div>


					<!-- Pembatas -->
					<div class="hr1" style="margin-bottom:14px;"></div>
					<p>Video yang menjelaskan perbedaan antara sekolah pada Sekolah Menengah Atas dengan Sekolah Menengah Kejuruan seperti <a href="http://www.smkbpi.sch.id/" target="_blank">SMK BPI Bandung</a>, SMK BISA !</p>
					<div class="hr1" style="margin-bottom:20px;"></div>
				</div>

				<!-- Video Profil -->
				<div class="col-md-6">
					<iframe width="600" height="350" src="https://www.youtube.com/embed/fqufyZnJtgk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

				</div>

			</div>

		</div>
	</div>
</div>

<!-- Postigan Terbaru -->
<div id="content">
	<!-- Tampilan Journal -->
	<div>
		<div class="container">
			<div class="row">

				<div class="col-md-4">
					<!-- Start Big Heading -->
					<div class="big-title">
						<h1>Journal <strong>SMK BPI</strong></h1>
						<p class="title-desc">Karya Tulis Ilmiah</p>
					</div>
					<!-- End Big Heading -->

					<!-- Some Text -->
					<p>Setiap pendidik di SMK BPI Bandung memiliki semangat juang tinggi untuk mewujudkan tujuan pendidikan nasional, salah satunya dengan melakukan penelitian. Berikut ini inovasi yang sudah kami lakukan sejauh ini.</p>
					<!-- Read more Journal -->
					<div class="hr1" style="margin-bottom:20px;"></div>
					<!-- <a href="<?php echo site_url('journal'); ?>" class="btn-system btn-small">Selengkapnya</a> -->
					<!-- Divider -->
					<div class="hr1" style="margin-bottom:10px;"></div>

				</div>

				<!-- journal -->
				<div class="col-md-8 col-sm-8 col-xs-12">
					<table id="journal" class="table table-striped table-bordered">
						<thead>
							<tr>
								<th scope="col">#</th>
								<th scope="col">Judul</th>
								<th scope="col">Penulis</th>
								<th scope="col">Berkas</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$i = 1;
							foreach ($journals as $journal) : ?>
								<tr>
									<th scope="row"><?= $i ?></th>
									<td><a href="<?= base_url('journal/index/' . $journal->id) ?>"><?= $journal->judul ?></a></td>
									<td><?= $journal->penulis ?></td>
									<td><a href="<?= base_url($journal->file_url) ?>" target="_blank" rel="noopener noreferrer">Unduh</a></td>
								</tr>
							<?php
								$i++;
							endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<br><br><br>

	<div class="container">
		<div class="page-content">

			<div class="recent-projects">
				<h4 class="title"><span>Postingan Terbaru</span></h4>
				<div class="projects-carousel touch-carousel">

					<!-- Foto Acara Terbaru update-->
					<?php foreach ($instas as $data) : ?>
						<div class="portfolio-item item">
							<div class="portfolio-border">
								<div class="portfolio-thumb">

									<blockquote class="instagram-media" data-instgrm-permalink="<?= $data->link ?>/?utm_source=ig_embed&amp;utm_campaign=loading" data-instgrm-version="13" style=" background:#FFF; border:0; border-radius:3px; box-shadow:0 0 1px 0 rgba(0,0,0,0.5),0 1px 10px 0 rgba(0,0,0,0.15); margin: 1px; max-width:326px; min-width:326px; padding:0; width:99.375%; width:-webkit-calc(100% - 2px); width:calc(100% - 2px);">
										<div style="padding:16px;">
											<a href="<?= $data->link ?>/?utm_source=ig_embed&amp;utm_campaign=loading" style=" background:#FFFFFF; line-height:0; padding:0 0; text-align:center; text-decoration:none; width:100%;" target="_blank">
												<div style=" display: flex; flex-direction: row; align-items: center;">
													<div style="background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 40px; margin-right: 14px; width: 40px;">
													</div>
													<div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center;">
														<div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 100px;">
														</div>
														<div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 60px;">
														</div>
													</div>
												</div>

												<div style="padding: 19% 0;"></div>
												<div style="display:block; height:50px; margin:0 auto 12px; width:50px;">
													<svg width="50px" height="50px" viewBox="0 0 60 60" version="1.1" xmlns="https://www.w3.org/2000/svg" xmlns:xlink="https://www.w3.org/1999/xlink">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<g transform="translate(-511.000000, -20.000000)" fill="#000000">
																<g>
																	<path d="M556.869,30.41 C554.814,30.41 553.148,32.076 553.148,34.131 C553.148,36.186 554.814,37.852 556.869,37.852 C558.924,37.852 560.59,36.186 560.59,34.131 C560.59,32.076 558.924,30.41 556.869,30.41 M541,60.657 C535.114,60.657 530.342,55.887 530.342,50 C530.342,44.114 535.114,39.342 541,39.342 C546.887,39.342 551.658,44.114 551.658,50 C551.658,55.887 546.887,60.657 541,60.657 M541,33.886 C532.1,33.886 524.886,41.1 524.886,50 C524.886,58.899 532.1,66.113 541,66.113 C549.9,66.113 557.115,58.899 557.115,50 C557.115,41.1 549.9,33.886 541,33.886 M565.378,62.101 C565.244,65.022 564.756,66.606 564.346,67.663 C563.803,69.06 563.154,70.057 562.106,71.106 C561.058,72.155 560.06,72.803 558.662,73.347 C557.607,73.757 556.021,74.244 553.102,74.378 C549.944,74.521 548.997,74.552 541,74.552 C533.003,74.552 532.056,74.521 528.898,74.378 C525.979,74.244 524.393,73.757 523.338,73.347 C521.94,72.803 520.942,72.155 519.894,71.106 C518.846,70.057 518.197,69.06 517.654,67.663 C517.244,66.606 516.755,65.022 516.623,62.101 C516.479,58.943 516.448,57.996 516.448,50 C516.448,42.003 516.479,41.056 516.623,37.899 C516.755,34.978 517.244,33.391 517.654,32.338 C518.197,30.938 518.846,29.942 519.894,28.894 C520.942,27.846 521.94,27.196 523.338,26.654 C524.393,26.244 525.979,25.756 528.898,25.623 C532.057,25.479 533.004,25.448 541,25.448 C548.997,25.448 549.943,25.479 553.102,25.623 C556.021,25.756 557.607,26.244 558.662,26.654 C560.06,27.196 561.058,27.846 562.106,28.894 C563.154,29.942 563.803,30.938 564.346,32.338 C564.756,33.391 565.244,34.978 565.378,37.899 C565.522,41.056 565.552,42.003 565.552,50 C565.552,57.996 565.522,58.943 565.378,62.101 M570.82,37.631 C570.674,34.438 570.167,32.258 569.425,30.349 C568.659,28.377 567.633,26.702 565.965,25.035 C564.297,23.368 562.623,22.342 560.652,21.575 C558.743,20.834 556.562,20.326 553.369,20.18 C550.169,20.033 549.148,20 541,20 C532.853,20 531.831,20.033 528.631,20.18 C525.438,20.326 523.257,20.834 521.349,21.575 C519.376,22.342 517.703,23.368 516.035,25.035 C514.368,26.702 513.342,28.377 512.574,30.349 C511.834,32.258 511.326,34.438 511.181,37.631 C511.035,40.831 511,41.851 511,50 C511,58.147 511.035,59.17 511.181,62.369 C511.326,65.562 511.834,67.743 512.574,69.651 C513.342,71.625 514.368,73.296 516.035,74.965 C517.703,76.634 519.376,77.658 521.349,78.425 C523.257,79.167 525.438,79.673 528.631,79.82 C531.831,79.965 532.853,80.001 541,80.001 C549.148,80.001 550.169,79.965 553.369,79.82 C556.562,79.673 558.743,79.167 560.652,78.425 C562.623,77.658 564.297,76.634 565.965,74.965 C567.633,73.296 568.659,71.625 569.425,69.651 C570.167,67.743 570.674,65.562 570.82,62.369 C570.966,59.17 571,58.147 571,50 C571,41.851 570.966,40.831 570.82,37.631"></path>
																</g>
															</g>
														</g>
													</svg>
												</div>

												<div style="padding-top: 8px;">
													<div style=" color:#3897f0; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:550; line-height:18px;"> Lihat postingan ini di Instagram
													</div>
												</div>

												<div style="padding: 12.5% 0;"></div>
												<div style="display: flex; flex-direction: row; margin-bottom: 14px; align-items: center;">
													<div>
														<div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(0px) translateY(7px);"></div>
														<div style="background-color: #F4F4F4; height: 12.5px; transform: rotate(-45deg) translateX(3px) translateY(1px); width: 12.5px; flex-grow: 0; margin-right: 14px; margin-left: 2px;"></div>
														<div style="background-color: #F4F4F4; border-radius: 50%; height: 12.5px; width: 12.5px; transform: translateX(9px) translateY(-18px);"></div>
													</div>
													<div style="margin-left: 8px;">
														<div style=" background-color: #F4F4F4; border-radius: 50%; flex-grow: 0; height: 20px; width: 20px;"></div>
														<div style=" width: 0; height: 0; border-top: 2px solid transparent; border-left: 6px solid #f4f4f4; border-bottom: 2px solid transparent; transform: translateX(16px) translateY(-4px) rotate(30deg)"></div>
													</div>
													<div style="margin-left: auto;">
														<div style=" width: 0px; border-top: 8px solid #F4F4F4; border-right: 8px solid transparent; transform: translateY(16px);"></div>
														<div style=" background-color: #F4F4F4; flex-grow: 0; height: 12px; width: 16px; transform: translateY(-4px);"></div>
														<div style=" width: 0; height: 0; border-top: 8px solid #F4F4F4; border-left: 8px solid transparent; transform: translateY(-4px) translateX(8px);"></div>
													</div>
												</div>
												<div style="display: flex; flex-direction: column; flex-grow: 1; justify-content: center; margin-bottom: 24px;">
													<div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; margin-bottom: 6px; width: 224px;"></div>
													<div style=" background-color: #F4F4F4; border-radius: 4px; flex-grow: 0; height: 14px; width: 144px;"></div>
												</div>
											</a>
											<p style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; line-height:17px; margin-bottom:0; margin-top:8px; overflow:hidden; padding:8px 0 7px; text-align:center; text-overflow:ellipsis; white-space:nowrap;">
												<a href="<?= $data->link ?>/?utm_source=ig_embed&amp;utm_campaign=loading" style=" color:#c9c8cd; font-family:Arial,sans-serif; font-size:14px; font-style:normal; font-weight:normal; line-height:17px; text-decoration:none;" target="_blank">Sebuah kiriman dibagikan oleh SMK BPI BANDUNG (@smkbpibandung)
												</a>
											</p>
										</div>
									</blockquote>

								</div>
							</div>
						</div>
					<?php endforeach; ?>

				</div>
			</div>
		</div>
	</div><br><br>


	<!-- berita -->
	<!-- <div id="content" style="margin-top:-55px;">
		<div class="container">
			<div class="latest-posts">
				<h4 class="classic-title"><span><b>Berita Terbaru</b></span></h4>
				<div class="latest-posts-classic custom-carousel touch-carousel" data-appeared-items="3">

					<?php foreach ($berita as $data) : ?>
						<div class="post-row item">
							<div class="left-meta-post">
								<div class="post-date"><span class="day"><?= date('d', strtotime($data->create_dt)) ?></span><span class="month"><?= date('M',  strtotime($data->create_dt)) ?></span></div>
								<div class="post-type"><i class="fa fa-picture-o"></i></div>
							</div>
							<h3 class="post-title"><a href="#"><?= $data->judul_berita ?></a></h3>
							<div class="post-content readmore">
								<p><?= $data->berita ?></p>
							</div>
						</div>
					<?php endforeach; ?>

				</div>
			</div>
		</div>
	</div> -->
	<!-- end of berita -->

	<div id="content" class="full-sections">
		<div class="section" style="padding-top:60px; padding-bottom:60px; border-top:1px solid #eee; border-bottom:1px solid #eee; background:#f9f9f9;">
			<div class="container">

				<div class="row">

					<div class="col-md-6">

						<div class="big-title">
							<h1><strong>Profil SMK BPI</strong> Bandung</h1>
							<p class="title-desc">Berkualitas, Terpercaya dan Berprestasi !</p>
						</div>

						<p>SMK BPI Bandung adalah sebuah sekolah kejuruan yang dibawahi <a href="http://main.bpiedu.id/" target="_blank">Yayasan Badan Perguruan Indonesia </a>, yang mempunyai 3 Program Studi keahlian yang sudah terakreditasi antara lain :</p>

						<!-- Pembatas -->
						<div class="hr1" style="margin-bottom:14px;"></div>

						<div class="row">
							<div class="col-md-7">
								<ul class="icons-list">
									<li><i class="fa fa-check-circle"></i> Otomatisasi dan Tata Kelola Perkantoran.</li>
									<li><i class="fa fa-check-circle"></i> Rekayasa Perangkat Lunak.</li>
									<li><i class="fa fa-check-circle"></i> Teknik Komputer Jaringan.</li>
								</ul>
							</div>
						</div>

						<div class="hr1" style="margin-bottom:20px;"></div>

						<!-- Read more Profil -->
						<a href="<?php echo site_url('home/profil'); ?>" class="btn-system btn-small">Read More Profil SMK BPI</a>

						<div class="hr1" style="margin-bottom:20px;"></div>
					</div>

					<!-- Video Profil -->
					<div class="col-md-6">
						<iframe width="600" height="350" src="https://www.youtube.com/embed/g8mZLdtctMM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					</div>

				</div>

			</div>
		</div>
	</div>

	<!-- Tampilan Animasi Text -->
	<div class="section purchase" style="<?php base_url(); ?>background: url(assets/images/parallax/tes.png) no-repeat;">
		<div class="container">
			<div class="section-video-content text-center">

				<!-- Tampilan Animasi Text -->
				<h1 class="fittext wite-text uppercase tlt" style="font-weight:400; ">
					<span class="texts">
						<span>Berakhlak Mulia</span>
						<span>Berprestasi</span>
						<span>Organisasi</span>
						<span>Inovatif</span>
						<span>Kreatif</span>
					</span>
					adalah Pedoman<br />SMK<strong> BPI</strong> Bandung
				</h1>
			</div>
		</div>
	</div>
	<!-- Pembatas-->
	<div class="hr1" style="margin-bottom:50px;"></div>

	<!-- Tampilan Alumni -->
	<div>
		<div class="container">
			<div class="row">

				<div class="col-md-3">
					<!-- Start Big Heading -->
					<div class="big-title">
						<h1>Alumni <strong>SMK BPI</strong></h1>
						<p class="title-desc">Alumni Side</p>
					</div>
					<!-- End Big Heading -->

					<!-- Some Text -->
					<p>Ini adalah Pesan Kesan Alumni setelah lulus dari SMK BPI Bandung.</p>
					<!-- Read more Alumni -->
					<div class="hr1" style="margin-bottom:20px;"></div>
					<a href="<?php echo site_url('alumni'); ?>" class="btn-system btn-small">Selengkapnya</a>
					<!-- Divider -->
					<div class="hr1" style="margin-bottom:10px;"></div>

				</div>
				<?php foreach ($alumnis as $alumni) : ?>
					<!-- Alumni 1 -->
					<div class="col-md-3 col-sm-6 col-xs-12">
						<div class="team-member">
							<!-- Alumni 1 -->
							<div class="member-photo">
								<img alt="" src="<?php echo base_url($alumni->image_url); ?>" />
								<div class="member-name"><?php echo $alumni->name; ?><span style="color:black; font-weight:500;"><?php echo $alumni->subtitle; ?></span></div>
							</div>
							<!-- Alumni 1 Words -->
							<div class="member-info">
								<blockquote>
									<p>"<?php echo $alumni->description; ?>."</p>
								</blockquote>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>
	</div>


	<!-- Pembatas -->
	<div class="hr1" id="mitra" style="margin-bottom:60px;"></div>

	<!-- test -->
	<!-- Tampilan Mitra Perusahaan -->
	<div class="partner">
		<div class="container">
			<div class="row">
				<div class="big-title text-center">
					<h1>Mitra <strong>Perusahaan</strong></h1>
					<p class="title-desc">Kita bekerja sama dan mitra dengan Perusahaan</p>
				</div>

				<!-- Carousel Mitra Perusahaan --->
				<div class="our-clients">
					<div class="clients-carousel custom-carousel touch-carousel navigation-3" data-appeared-items="6" data-navigation="true">

						<!-- Perusahaan 1 -->
						<div class="client-item item">
							<a href="https://www.smooets.com/"><img src="<?php echo base_url(); ?>assets/images/smooets.png" alt="" style="margin-top:20px;" /></a>
						</div>

						<!-- Perusahaan 2 -->
						<div class="client-item item">
							<a href="http://www.mikrotik.com/"><img src="<?php echo base_url(); ?>assets/images/mikrotik.png" alt="" /></a>
						</div>

						<!-- Perusahaan 3 -->
						<div class="client-item item">
							<a href="https://www.skyline.net.id/"><img src="<?php echo base_url(); ?>assets/images/skyline.png" alt="" /></a>
						</div>

						<!-- Perusahaan 4 -->
						<div class="client-item item">
							<a href="http://www.cgs.co.id/newcgs/"><img src="<?php echo base_url(); ?>assets/images/cgs.jpg" alt="" /></a>
						</div>

						<!-- Perusahaan 5 -->
						<!--
					  <div class="client-item item">
						<a href="http://www.axiooworld.com/"><img src="<?php echo base_url(); ?>assets/images/axioo.png" alt="" /></a>
					  </div>
					  -->

						<!-- Perusahaan 6 -->
						<div class="client-item item">
							<a href="http://www.melsa.net.id/"><img src="<?php echo base_url(); ?>assets/images/melsa.png" alt="" /></a>
						</div>

						<!-- Perusahaan 7 -->
						<div class="client-item item">
							<a href="http://www.amikom.ac.id/"><img src="<?php echo base_url(); ?>assets/images/amikom1.png" alt="" /></a>
						</div>

						<!-- Perusahaan 8 -->
						<div class="client-item item">
							<a href="http://www.msvpictures.com/"><img src="<?php echo base_url(); ?>assets/images/msv.png" alt="" /></a>
						</div>

						<!-- Perusahaan 9 -->
						<div class="client-item item">
							<a href="http://www.belajarmikrotik.com/"><img src="<?php echo base_url(); ?>assets/images/belajar_mikrotik.png" alt="" /></a>
						</div>

						<!-- Perusahaan 10 -->
						<div class="client-item item">
							<a href="https://www.inti.co.id/"><img src="<?php echo base_url(); ?>assets/images/inti.png" alt="" /></a>
						</div>

						<!-- Perusahaan 11 -->
						<div class="client-item item">
							<a href="https://www.itb.ac.id/"><img src="<?php echo base_url(); ?>assets/images/itb.jpg" alt="" /></a>
						</div>

						<!-- Perusahaan 12 -->
						<div class="client-item item">
							<a href="http://aspapi.org/"><img src="<?php echo base_url(); ?>assets/images/aspapi.jpg" alt="" /></a>
						</div>



					</div>
				</div>
			</div>
		</div>
	</div>

	<!-- instagram script -->
	<script async src="//www.instagram.com/embed.js"></script>

	<script>
		$(document).ready(function() {
			$('#journal').DataTable();
		});
	</script>