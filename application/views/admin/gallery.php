<!-- tgl_posting Content -->
	<div id="content">
		<div class="container">
            <div class="page-content">
                <div class="row">
                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-8">
                        <p class="text-uppercase" style="margin-top:10px">GALERI FOTO</p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 text-right">
                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            <i class="fa fa-plus"></i> Tambah Gambar
                        </button>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="collapse" id="collapseExample">
                            <div class="well">
                                <form method="POST" action="<?= base_url('adminController/save_galeri') ?>" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>Judul</label>
                                        <input type="text" name="judul_galeri" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Deskripsi</label>
                                        <textarea class="form-control" name="deskripsi" rows="3"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Upload gambar</label>
                                        <div class="input-group" style="width:10px">
                                            <input type="file" class="form-control-file" name="foto">
                                            <span class="input-group-addon"><i class="fa fa-file"></i></span>
                                        </div>
                                    </div>
                                    <div class="form-group text-right">
                                        <button type="submit" class="btn btn-success">SIMPAN</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="flash-data" data-flashdata="<?=$this->session->flashdata('notif')?>"></div>
                <div class="row">
                    <div class="table-responsive">
                        <table class="table table-hover" id="mydata">
                            <thead>
                                <tr>
                                    <th class="text-center">NO</th>
                                    <th>Judul</th>
                                    <th>Gambar</th>
                                    <th>Keterangan</th>
                                    <th>Tanggal</th>
                                    <th class="text-center"><span class="fa fa-cog"></span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    $no = 1;
                                    foreach ($model as $data) :
                                    ?>
                                    <tr>
                                        <td class="text-center"><?= $no; ?></td>
                                        <td><?= $data->judul_galeri; ?></td>
                                        <td>
                                        <?php if($data->foto){?>
                                            <img src="<?= base_url('assets/images/galeri/'.$data->foto); ?>" alt="foto" class="img-fluid" style="width:50px;height:40px;border-radius:10px">
                                        <?php }else{?>
                                            <i class="fa fa-question-circle fa-2x" style="margin-left:10px"></i>
                                        <?php }?>
                                        </td>
                                        <td class="read"><?= $data->deskripsi; ?></td>
                                        <td><?= $data->tgl_posting; ?></td>
                                        <td colspan="2" class="text-center">
                                            <a href="javascript:void();" data-id="<?= $data->id_galeri; ?>" data-judul_galeri="<?= $data->judul_galeri; ?>" data-deskripsi="<?= $data->deskripsi; ?>" data-tgl_posting="<?= $data->tgl_posting; ?>" data-toggle="modal" data-target="#form-edit" class="btn btn-warning"><span class="fa fa-pencil"></span></a>
                                            <a href="<?= base_url('adminController/delete_galeri/'.$data->id_galeri) ?>" data-id="<?= $data->id_galeri; ?>" class="btn btn-danger btn-delete"><span class="fa fa-trash"></span></a>
                                        </td>
                                    </tr>
                                <?php
                                    $no++;
                                endforeach;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
		</div>
	</div>
    <!-- End Content -->
    <!-- ====================================== MODAL =============================================== -->
    <div class="modal fade" id="form-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top:100px">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-judul_galeri" id="exampleModalLabel">Data Acara</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
                <div class="modal-body">
                    <form method="POST" action="<?= base_url('adminController/update_galeri') ?>" enctype="multipart/form-data">
                        <input type="hidden" id="id" name="id_galeri">
                        <div class="form-group">
                            <label>Judul/Label foto</label>
                            <input type="text" class="form-control" id="judul_galeri" name="judul_galeri">
                        </div>
                        <div class="form-group">
                            <label>Keterangan foto</label>
                            <textarea class="form-control" id="deskripsi" name="deskripsi" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label>Tanggal posting</label>
                            <input type="text" class="form-control date" id="tgl_posting" name="tgl_posting">
                        </div>
                        <div class="form-group">
                            <label>Upload gambar</label>
                            <div class="input-group" style="width:10px">
                                <input type="file" name="foto" class="form-control-file btn btn-outline-primary">
                                <span class="input-group-addon"><i class="fa fa-file"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Ubah</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#form-edit').on('show.bs.modal', function(event) {
            var div = $(event.relatedTarget)
            var modal = $(this)

            modal.find('#id').attr("value", div.data('id'));
            modal.find('#judul_galeri').attr("value", div.data('judul_galeri'));
            modal.find('#deskripsi').attr("value", div.data('deskripsi'));
            modal.find('#tgl_posting').attr("value", div.data('tgl_posting'));
            modal.find('#end').attr("value", div.data('end'));
        });
    </script>