<!-- Start Content -->

	<div id="content">

		<div class="container">

            <div class="page-content">

                <div class="row">

                    <div class="col-lg-9 col-md-9 col-sm-8 col-xs-8">

                        <p class="text-uppercase" style="margin-top:10px">DATA EVENTS</p>

                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 text-right">

                        <button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">

                            <i class="fa fa-plus"></i> Buat Acara

                        </button>

                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">

                        <div class="collapse" id="collapseExample">

                            <div class="well">

                                <form method="POST" action="<?= base_url('adminController/save_acara') ?>" enctype="multipart/form-data">

                                    <div class="form-group">

                                        <label>Judul Acara</label>

                                        <input type="text" name="title" class="form-control">

                                    </div>

                                    <div class="form-group">

                                        <label>Deskripsi Acara</label>

                                        <textarea class="form-control" name="description" rows="3"></textarea>

                                    </div>

                                    <div class="row">

                                        <div class="form-group col-lg-6">

                                            <label>Mulai Acara</label>

                                            <div class="input-group">

                                                <input type="text" name="start" class="form-control bg-light border-0 small" id="StartDate" value="<?= date('m/d/Y') ?>" aria-label="Search" aria-describedby="basic-addon2">

                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                            </div>

                                        </div>

                                        <div class="form-group col-lg-6">

                                            <label>Akhir Acara *</label>

                                            <div class="input-group">

                                                <input type="text" name="end" class="form-control bg-light border-0 small" id="EndDate" value="<?= date('m/d/Y') ?>" aria-label="Search" aria-describedby="basic-addon2">

                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                            </div>

                                        </div>

                                    </div>

                                    <div class="form-group">

                                        <label>Upload gambar</label>

                                        <div class="input-group" style="width:10px">

                                            <input type="file" class="form-control-file" name="image">

                                            <span class="input-group-addon"><i class="fa fa-file"></i></span>

                                        </div>

                                    </div>

                                    <div class="form-group text-right">

                                        <button type="submit" class="btn btn-success">SIMPAN</button>

                                    </div>

                                </form>

                            </div>

                        </div>

                    </div>

                </div>

                <hr>

                <div class="flash-data" data-flashdata="<?=$this->session->flashdata('notif')?>"></div>

                <div class="row">

                    <div class="table-responsive">

                        <table class="table table-hover" id="mydata">

                            <thead>

                                <tr>

                                    <th class="text-center">NO</th>

                                    <th>Judul Acara</th>

                                    <th>Deskripsi Acara</th>

                                    <th>Gambar</th>

                                    <th>Mulai acara</th>

                                    <th>Akhir acara</th>

                                    <th class="text-center"><span class="fa fa-cog"></span></th>

                                </tr>

                            </thead>

                            <tbody>

                                <?php

                                    $no = 1;

                                    foreach ($model as $data) :

                                        ?>

                                        <tr>

                                            <td class="text-center"><?= $no; ?></td>

                                            <td><?= $data->title; ?></td>

                                            <td class="read"><?= $data->description; ?></td>

                                            <td>

                                            <?php if($data->image == '' or !$data->image){?>

                                                <i class="fa fa-question-circle fa-2x" style="margin-left:10px"></i>

                                            <?php }else{?>

                                                <img src="<?= base_url('assets/images/events/'.$data->image); ?>" alt="image" class="img-fluid" style="width:50px;height:40px;border-radius:10px">

                                            <?php }?>

                                            </td>

                                            <td><?= $data->start; ?></td>

                                            <td><?= $data->end; ?></td>

                                            <td colspan="2" class="text-center">

                                                <a href="javascript:void();" data-id="<?= $data->id; ?>" data-title="<?= $data->title; ?>" data-description="<?= $data->description; ?>" data-start="<?= $data->start; ?>" data-end="<?= $data->end; ?>" data-toggle="modal" data-target="#form-edit" class="btn btn-warning"><span class="fa fa-pencil"></span></a>

                                                <a href="<?= base_url('adminController/delete_acara/'.$data->id) ?>" data-id="<?= $data->id; ?>" class="btn btn-danger btn-delete"><span class="fa fa-trash"></span></a>

                                            </td>

                                        </tr>

                                    <?php

                                        $no++;

                                    endforeach;

                                    ?>

                            </tbody>

                        </table>

                    </div>

                </div>



            </div>

		</div>

	</div>

    <!-- End Content -->

    <!-- ====================================== MODAL =============================================== -->

    <div class="modal fade" id="form-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top:100px">

    <div class="modal-dialog" role="document">

        <div class="modal-content">

        <div class="modal-header">

            <h5 class="modal-title" id="exampleModalLabel">Data Acara</h5>

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">

            <span aria-hidden="true">&times;</span>

            </button>

        </div>

                <div class="modal-body">

                    <form method="POST" action="<?= base_url('adminController/update_acara') ?>" enctype="multipart/form-data">
                        <input type="hidden" id="id" name="id">

                        <div class="form-group">

                            <label>Judul/tema Acara</label>

                            <input type="text" class="form-control" id="title" name="title">

                        </div>

                        <div class="form-group">

                            <label>Deskripsi Acara</label>

                            <textarea class="form-control" id="description" name="description" rows="3"></textarea>

                        </div>

                        <div class="row">

                            <div class="form-group col-lg-6">

                                <label>Mulai Acara</label>

                                <input type="text" class="form-control date" id="start" name="start">

                            </div>

                            <div class="form-group col-lg-6">

                                <label>Akhir Acara</label>

                                <input type="text" class="form-control date" id="end" name="end">

                            </div>

                        </div>

                        <div class="form-group">

                            <label>Upload gambar</label>

                            <div class="input-group" style="width:10px">

                                <input type="file" name="image" class="form-control-file btn btn-outline-primary">

                                <span class="input-group-addon"><i class="fa fa-file"></i></span>

                            </div>

                        </div>

                    </div>

                    <div class="modal-footer">

                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>

                        <button type="submit" class="btn btn-primary">Ubah</button>

                    </form>

                </div>

            </div>

        </div>

    </div>

    <script>

        $('#form-edit').on('show.bs.modal', function(event) {

            var div = $(event.relatedTarget)

            var modal = $(this)



            modal.find('#id').attr("value", div.data('id'));

            modal.find('#title').attr("value", div.data('title'));

            modal.find('#description').attr("value", div.data('description'));

            modal.find('#start').attr("value", div.data('start'));

            modal.find('#end').attr("value", div.data('end'));

        });

    </script>