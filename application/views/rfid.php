    <div id="content">

    	<div class="container">

    		<div class="page-content">

    			<!-- <h4 class="classic-title"><span><i class="fa fa-trophy"></i> Prestasi yang diraih siswa SMK BPI</span></h4> -->

    			<div class="big-title text-center">
    				<h1>Presensi Siswa/i <strong>Hari Ini</strong></h1>
    				<p class="title-desc">Data Presensi Online live</p>
    			</div>

    			<div class="row" style="margin-top:20px; ">

    				<div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:20px; ">
    					<table id="presensi" class="table table-striped table-bordered" style="width:100%">
    						<thead>
    							<tr>
    								<th>rfid</th>
    								<th>Nama Siswa</th>
    								<th>Tanggal</th>
    								<th>Waktu Masuk</th>
    								<th>Waktu di Sekolah</th>
    								<th>Waktu Keluar</th>
    							</tr>
    						</thead>
    						<tbody>
    							<?php
								//var_dump($presensi[0]->Timestamp);
								// echo substr($presensi[0]->Timestamp, 0, 10);
								//echo date("m/d/Y");
								foreach ($rfids as $p) {
								?>
    								<tr>
    									<td><?= $p->rfid; ?></td>
    									<td><?= $p->name; ?></td>
    									<td><?= $p->tanggal; ?></td>
    									<td><?= $p->tgl_masuk; ?></td>
    									<td><?= $p->diff; ?></td>
    									<td><?= $p->tgl_keluar; ?></td>
    								</tr>
    							<?php
								}
								?>

    						</tbody>
    					</table>
    				</div>

    			</div>


    		</div>
    	</div>
    </div>

    <script>
    	$(document).ready(function() {
    		$('#presensi').DataTable();
    	});
    </script>