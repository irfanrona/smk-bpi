    <div id="content">

    	<div class="container">

    		<div class="page-content">

    			<!-- <h4 class="classic-title"><span><i class="fa fa-trophy"></i> Prestasi yang diraih siswa SMK BPI</span></h4> -->

    			<div class="big-title text-center">
    				<h1>Journal Pendidik <strong>SMK BPI</strong></h1>
    				<p class="title-desc">Bermartabat, Berkualitas, dan Terpercaya</p>
    			</div>

    			<!-- journal -->
    			<div class="col-md-8 col-sm-8 col-xs-12">
    				<table id="journal" class="table table-striped table-bordered">
    					<thead>
    						<tr>
    							<th scope="col">#</th>
    							<th scope="col">Judul</th>
    							<th scope="col">Penulis</th>
    							<th scope="col">Berkas</th>
    						</tr>
    					</thead>
    					<tbody>
    						<?php
							$i = 1;
							foreach ($journals as $journal) : ?>
    							<tr>
    								<th scope="row"><?= $i ?></th>
    								<td><?= $journal->judul ?></td>
    								<td><?= $journal->penulis ?></td>
    								<td>Unduh</td>
    							</tr>
    						<?php
								$i++;
							endforeach; ?>
    					</tbody>
    				</table>
    			</div>
    		</div>
    	</div>
    </div>