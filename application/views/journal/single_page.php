<!doctype html>
<!-- Agenda -->
<!-- Start Content -->
<div id="content" style="margin-top:9px;">
	<div class="container">
		<div class="project-page row">

			<!-- Start Single Project Slider -->
			<div class="project-media col-md-8">
				<div>
					<?= $journal->abstrak ?>
				</div>
			</div>
			<!-- End Single Project Slider -->

			<!-- Start Project Content -->
			<div class="project-content col-md-4">
				<div>
					<h5> <?= $journal->judul ?></h5>
				</div>
				<ul>
					<li><span class="fa fa-calendar"> <?= $journal->modified_date ?> | <span class="fa fa-eye"> <?= $journal->count ?></li>
				</ul>
				<h6><a class="btn btn-primary" href="<?= base_url($journal->file_url) ?>" target="_blank"><span class="fa fa-download"></span> Download Jurnal</a></h6>

			</div>
			<!-- End Project Content -->

		</div>

	</div>
</div>
<!-- End Recent Projects Carousel -->