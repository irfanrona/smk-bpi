    <div id="content">

    	<div class="container">

    		<div class="page-content">

    			<div class="row">
    				<div class="col-lg-9 col-md-9 col-sm-8 col-xs-8">

    					<p class="text-uppercase" style="margin-top:10px">DATA Karya Tulis Ilmiah</p>

    				</div>

    				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 text-right">

    					<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">

    						<i class="fa fa-plus"></i> Tambah Karya

    					</button>

    				</div>

    				<div class="col-md-12 col-sm-12 col-xs-12">

    					<div class="collapse" id="collapseExample">
    						<div class="well">
    							<form action="<?= base_url('journal/insert') ?>" method="post" enctype="multipart/form-data">
    								<div class="form-group">
    									<label for="name">Judul Karya</label>
    									<input class="form-control" type="text" name="judul" id="judul" placeholder="Rancang Bangun Sistem Ketahanan..." required>
    								</div>
    								<div class="form-group">
    									<label for="name">Nama Penulis</label>
    									<input class="form-control" type="text" name="penulis" id="penulis" placeholder="Ade Aso, S.Pd" required>
    								</div>
    								<div class="form-group">

    									<label>Upload karya bentuk PDF</label>

    									<div class="input-group" style="width:10px">

    										<input type="file" class="form-control-file" name="file">

    										<span class="input-group-addon"><i class="fa fa-file"></i></span>

    									</div>

    								</div>

    								<div class="form-group">

    									<label>Abstrak</label>

    									<textarea id="summernote" class="form-control" name="abstrak" rows="5"></textarea>

    								</div>
    								<input type="hidden" name="show" id="show" value="true">
    								<div class="form-group text-right">

    									<button type="submit" class="btn btn-success">Tambah Karya</button>

    								</div>
    							</form>

    						</div>
    					</div>
    				</div>

    			</div>

    			<hr>
    			<div class="flash-data" data-flashdata="<?= $this->session->flashdata('notif') ?>"></div>

    			<div class="row">

    				<div class="table-responsive">

    					<table class="table table-hover" id="mydata">

    						<thead>

    							<tr>

    								<th class="text-center">NO</th>

    								<th>Judul Karya</th>

    								<th>Penulis</th>

    								<th>Tampilkan</th>

    								<th>Dokumen</th>

    								<th class="text-center"><span class="fa fa-cog"></span></th>

    							</tr>

    						</thead>

    						<tbody>

    							<?php

								$no = 1;

								foreach ($journals as $data) :

								?>

    								<tr>

    									<td class="text-center"><?= $no; ?></td>

    									<td><?= $data->judul; ?></td>

    									<td><?= $data->penulis; ?></td>

    									<td>
    										<?php if ($data->show == 'true') { ?>
    											<span class="fa fa-eye">
    											<?php } else { ?>
    												<span class="fa fa-eye-slash">
    												<?php } ?>
    									</td>

    									<td>

    										<?php if ($data->file_url == '' or !$data->file_url) { ?>

    											<i class="fa fa-question-circle fa-2x" style="margin-left:10px"></i>

    										<?php } else { ?>

    											<a href="<?= base_url($data->file_url) ?>" target="_blank" rel="noopener noreferrer">Unduh</a>

    										<?php } ?>

    									</td>

    									<td colspan="2" class="text-center">

    										<!-- <a href="javascript:void();" data-id="<?= $data->id; ?>" data-judul="<?= $data->judul; ?>" data-penulis="<?= $data->penulis; ?>" data-show="<?= $data->show; ?>" data-toggle="modal" data-target="#form-edit" class="btn btn-warning"><span class="fa fa-pencil"></span></a> -->

    										<a href="<?= base_url('journal/delete/' . $data->id) ?>" data-id="<?= $data->id; ?>" class="btn btn-danger btn-delete"><span class="fa fa-trash"></span></a>

    									</td>

    								</tr>

    							<?php

									$no++;

								endforeach;

								?>

    						</tbody>

    					</table>

    				</div>

    			</div>

    		</div>
    	</div>
    </div>

    <!-- End Content -->

    <!-- ====================================== MODAL =============================================== -->

    <div class="modal fade" id="form-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top:100px">

    	<div class="modal-dialog" role="document">

    		<div class="modal-content">

    			<div class="modal-header">

    				<h5 class="modal-title" id="exampleModalLabel">Data Journal</h5>

    				<button type="button" class="close" data-dismiss="modal" aria-label="Close">

    					<span aria-hidden="true">&times;</span>

    				</button>

    			</div>

    			<div class="modal-body">

    				<form method="POST" action="<?= base_url('journal/update') ?>" enctype="multipart/form-data">
    					<input type="hidden" id="id" name="id">

    					<div class="form-group">

    						<label>Judul Karya</label>

    						<input type="text" class="form-control" id="judul" name="judul">

    					</div>

    					<div class="form-group">

    						<label>Penulis</label>

    						<input type="text" class="form-control" id="penulis" name="penulis">

    					</div>

    					<div class="form-group">

    						<label>Upload Karya</label>

    						<div class="input-group" style="width:10px">

    							<input type="file" name="file" class="form-control-file btn btn-outline-primary">

    							<span class="input-group-addon"><i class="fa fa-file"></i></span>

    						</div>

    					</div>

    					<div class="form-group">

    						<label>Abstrak</label>

    						<textarea id="summernote_modal" class="form-control" name="abstrak" rows="5"></textarea>

    					</div>

    					<div class="form-group">

    						<label>Tampilkan di Beranda?</label>
    						<br>

    						<input type="radio" id="show_true" name="show" value="true">
    						<label for="show_true">ya</label><br>

    						<input type="radio" id="show_false" name="show" value="false">
    						<label for="show_false">tidak</label><br>

    					</div>

    			</div>

    			<div class="modal-footer">

    				<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>

    				<button type="submit" class="btn btn-primary">Ubah</button>

    				</form>

    			</div>

    		</div>

    	</div>

    </div>

    <script>
    	$(document).ready(function() {
    		$('#summernote').summernote({
    			height: 200
    		});
    	});
    	$('#form-edit').on('show.bs.modal', function(event) {

    		var div = $(event.relatedTarget)

    		var modal = $(this)

    		var markupStr = $('#summernote').summernote('code');
    		$('#summernote_modal').summernote('code', markupStr);

    		modal.find('#id').attr("value", div.data('id'));

    		modal.find('#judul').attr("value", div.data('judul'));

    		modal.find('#penulis').attr("value", div.data('penulis'));

    		modal.find('#abstrak').attr("value", div.data('abstrak'));

    		if (div.data('show') == true) {
    			modal.find('#show_true').prop("checked", true);
    		} else {
    			modal.find('#show_false').prop("checked", true);
    		}

    	});
    </script>