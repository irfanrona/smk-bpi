    <div id="content">

    	<div class="container">

    		<div class="page-content">

    			<!-- <h4 class="classic-title"><span><i class="fa fa-trophy"></i> Prestasi yang diraih siswa SMK BPI</span></h4> -->

    			<div class="big-title text-center">
    				<h1>Alumni Siswa/i <strong>SMK BPI</strong></h1>
    				<p class="title-desc">Bermartabat, Berkualitas, dan Terpercaya</p>
    			</div>



    			<?php foreach ($alumnis as $alumni) : ?>
    				<div class="col-md-4 col-sm-6 col-xs-12" style="margin-top:20px; ">
    					<div class="team-member">
    						<div class="member-photo text-center">
    							<img class="rounded mx-auto d-block" alt="" style="height:400px" src="<?php echo base_url($alumni->image_url); ?>" />
    							<div class="member-name"><?php echo $alumni->name; ?><span style="color:black; font-weight:500"><?php echo $alumni->subtitle; ?></span></div>
    						</div>
    					</div>
    					<div class="member-info">
    						<p>
    							<!-- <blockquote class="blockquote text-center">" <?php echo $alumni->description; ?>."</blockquote> -->
    						</p>
    					</div>
    				</div>
    			<?php endforeach; ?>

    		</div>
    	</div>
    </div>