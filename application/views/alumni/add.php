    <div id="content">

    	<div class="container">

    		<div class="page-content">

    			<div class="row">
    				<div class="col-lg-9 col-md-9 col-sm-8 col-xs-8">

    					<p class="text-uppercase" style="margin-top:10px">DATA ALUMNI</p>

    				</div>

    				<div class="col-lg-3 col-md-3 col-sm-4 col-xs-4 text-right">

    					<button class="btn btn-primary" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">

    						<i class="fa fa-plus"></i> Tambah Alumni

    					</button>

    				</div>

    				<div class="col-md-12 col-sm-12 col-xs-12">

    					<div class="collapse" id="collapseExample">
    						<div class="well">
    							<form action="<?= base_url('alumni/insert') ?>" method="post" enctype="multipart/form-data">
    								<div class="form-group">
    									<label for="name">Nama</label>
    									<input class="form-control" type="text" name="name" id="name" placeholder="Nama Alumni" required>
    								</div>
    								<div class="form-group">
    									<label for="name">Tempat Bekerja</label>
    									<input class="form-control" type="text" name="subtitle" id="subtitle" placeholder="PT. Smooets / Universitas Indonesia" required>
    								</div>
    								<div class="form-group">
    									<label for="name">Quote</label>
    									<textarea class="form-control" name="description" rows="3" placeholder="Setelah lulus dari BPI . . ." required></textarea>
    								</div>
    								<div class="form-group">

    									<label>Upload gambar</label>

    									<div class="input-group" style="width:10px">

    										<input type="file" class="form-control-file" name="image">

    										<span class="input-group-addon"><i class="fa fa-file"></i></span>

    									</div>

    								</div>
    								<input type="hidden" name="show" id="show" value="false">
    								<div class="form-group text-right">

    									<button type="submit" class="btn btn-success">Tambah Alumni</button>

    								</div>
    							</form>

    						</div>
    					</div>
    				</div>

    			</div>

    			<hr>
    			<div class="flash-data" data-flashdata="<?= $this->session->flashdata('notif') ?>"></div>

    			<div class="row">

    				<div class="table-responsive">

    					<table class="table table-hover" id="mydata">

    						<thead>

    							<tr>

    								<th class="text-center">NO</th>

    								<th>Nama Alumni</th>

    								<th>Tempat Sekarang</th>

    								<th>Quote</th>

    								<th>Tampilkan</th>

    								<th>Foto</th>

    								<th class="text-center"><span class="fa fa-cog"></span></th>

    							</tr>

    						</thead>

    						<tbody>

    							<?php

								$no = 1;

								foreach ($alumnis as $data) :

								?>

    								<tr>

    									<td class="text-center"><?= $no; ?></td>

    									<td><?= $data->name; ?></td>

    									<td><?= $data->subtitle; ?></td>

    									<td class="read"><?= $data->description; ?></td>

    									<td>
    										<?php if ($data->show == 'true') { ?>
    											<span class="fa fa-eye">
    											<?php } else { ?>
    												<span class="fa fa-eye-slash">
    												<?php } ?>
    									</td>

    									<td>

    										<?php if ($data->image_url == '' or !$data->image_url) { ?>

    											<i class="fa fa-question-circle fa-2x" style="margin-left:10px"></i>

    										<?php } else { ?>

    											<img src="<?= base_url($data->image_url); ?>" alt="image" class="img-fluid" style="width:50px;height:40px;border-radius:10px">

    										<?php } ?>

    									</td>

    									<td colspan="2" class="text-center">

    										<a href="javascript:void();" data-id="<?= $data->id; ?>" data-name="<?= $data->name; ?>" data-subtitle="<?= $data->subtitle; ?>" data-description="<?= $data->description; ?>" data-show="<?= $data->show; ?>" data-toggle="modal" data-target="#form-edit" class="btn btn-warning"><span class="fa fa-pencil"></span></a>

    										<a href="<?= base_url('alumni/delete/' . $data->id) ?>" data-id="<?= $data->id; ?>" class="btn btn-danger btn-delete"><span class="fa fa-trash"></span></a>

    									</td>

    								</tr>

    							<?php

									$no++;

								endforeach;

								?>

    						</tbody>

    					</table>

    				</div>

    			</div>

    		</div>
    	</div>
    </div>

    <!-- End Content -->

    <!-- ====================================== MODAL =============================================== -->

    <div class="modal fade" id="form-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top:100px">

    	<div class="modal-dialog" role="document">

    		<div class="modal-content">

    			<div class="modal-header">

    				<h5 class="modal-title" id="exampleModalLabel">Data Alumni</h5>

    				<button type="button" class="close" data-dismiss="modal" aria-label="Close">

    					<span aria-hidden="true">&times;</span>

    				</button>

    			</div>

    			<div class="modal-body">

    				<form method="POST" action="<?= base_url('alumni/update') ?>" enctype="multipart/form-data">
    					<input type="hidden" id="id" name="id">

    					<div class="form-group">

    						<label>Nama Alumni</label>

    						<input type="text" class="form-control" id="name" name="name">

    					</div>

    					<div class="form-group">

    						<label>Tempat Sekarang</label>

    						<input type="text" class="form-control" id="subtitle" name="subtitle">

    					</div>

    					<div class="form-group">

    						<label>Quote</label>

    						<textarea class="form-control" id="description" name="description" rows="3"></textarea>

    					</div>

    					<div class="form-group">

    						<label>Upload gambar</label>

    						<div class="input-group" style="width:10px">

    							<input type="file" name="image" class="form-control-file btn btn-outline-primary">

    							<span class="input-group-addon"><i class="fa fa-file"></i></span>

    						</div>

    					</div>

    					<div class="form-group">

    						<label>Tampilkan di Beranda? (hanya 3 alumni)</label>
    						<br>

    						<input type="radio" id="show_true" name="show" value="true">
    						<label for="show_true">ya</label><br>

    						<input type="radio" id="show_false" name="show" value="false">
    						<label for="show_false">tidak</label><br>

    					</div>

    			</div>

    			<div class="modal-footer">

    				<button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>

    				<button type="submit" class="btn btn-primary">Ubah</button>

    				</form>

    			</div>

    		</div>

    	</div>

    </div>

    <script>
    	$('#form-edit').on('show.bs.modal', function(event) {

    		var div = $(event.relatedTarget)

    		var modal = $(this)

    		modal.find('#id').attr("value", div.data('id'));

    		modal.find('#name').attr("value", div.data('name'));

    		modal.find('#subtitle').attr("value", div.data('subtitle'));

    		modal.find('#description').attr("value", div.data('description'));

    		if (div.data('show') == true) {
    			modal.find('#show_true').prop("checked", true);
    		} else {
    			modal.find('#show_false').prop("checked", true);
    		}

    	});
    </script>