    <div id="content">

    	<div class="container">

    		<div class="page-content">

    			<!-- <h4 class="classic-title"><span><i class="fa fa-trophy"></i> Prestasi yang diraih siswa SMK BPI</span></h4> -->

    			<div class="big-title text-center">
    				<h1>Presensi Siswa/i <strong>Hari Ini</strong></h1>
    				<p class="title-desc">Data Presensi Online live</p>
    			</div>

    			<div class="row" style="margin-top:20px; ">

    				<div class="col-md-12 col-sm-12 col-xs-12" style="margin-top:20px; ">
    					<table id="presensi" class="table table-striped table-bordered" style="width:100%">
    						<thead>
    							<tr>
    								<th>Waktu</th>
    								<th>Kelas</th>
    								<th>Nama</th>
    							</tr>
    						</thead>
    						<tbody>
    							<?php
								$presensi = json_decode(($presensi));
								//var_dump($presensi[0]->Timestamp);
								// echo substr($presensi[0]->Timestamp, 0, 10);
								//echo date("m/d/Y");
								foreach ($presensi as $p) {
									if (date("m/d/Y", strtotime($p->Timestamp)) == date("m/d/Y")) {
										$nama = "";
										if ($p->{'X TKJ'} != "") {
											$nama = $p->{'X TKJ'};
										} else
										if ($p->{'X RPL'} != "") {
											$nama = $p->{'X RPL'};
										} else
										if ($p->{'X OTKP'} != "") {
											$nama = $p->{'X OTKP'};
										} else
										if ($p->{'XI TKJ'} != "") {
											$nama = $p->{'XI TKJ'};
										} else
										if ($p->{'XI RPL'} != "") {
											$nama = $p->{'XI RPL'};
										} else
										if ($p->{'XI OTKP'} != "") {
											$nama = $p->{'XI OTKP'};
										} else
										if ($p->{'XII TKJ'} != "") {
											$nama = $p->{'XII TKJ'};
										} else
										if ($p->{'XII RPL'} != "") {
											$nama = $p->{'XII RPL'};
										} else
										if ($p->{'XII OTKP'} != "") {
											$nama = $p->{'XII OTKP'};
										}
								?>
    									<tr>
    										<td><?= $p->Timestamp; ?></td>
    										<td><?= $p->Kelas; ?></td>
    										<td><?= $nama; ?></td>
    									</tr>
    							<?php
									}
								}
								?>

    						</tbody>
    						<tfoot>
    							<tr>
    								<th>Waktu</th>
    								<th>Kelas</th>
    								<th>Nama</th>
    							</tr>
    						</tfoot>
    					</table>
    				</div>

    			</div>


    		</div>
    	</div>
    </div>

    <script>
    	$(document).ready(function() {
    		$('#presensi').DataTable();
    	});
    </script>