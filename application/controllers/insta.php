<?php if (! defined('BASEPATH')) exit ('No direct script access allowed');
	class Insta extends CI_Controller{

		public function __construct(){

			parent:: __construct();
			$this->load->model('insta_model');

		}

		public function index(){

			$data['title'] = 'SMK BPI Bandung';
			$data['instas']= $this->insta_model->getInsta();

			$this->load->view('master/header', $data);
			$this->load->view('master/navbar');

			$this->load->view('insta/insta', $data);

			$this->load->view('master/footer');
		}



	}

?>