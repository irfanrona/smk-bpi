<?php if (!defined('BASEPATH')) exit('No direct script access allowed');



class Api extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();

        $this->load->model('admin');
    }

    public function createInsta()
    {

        $username = "adminteachSMK";
        $password = $this->input->post('password');
        $pass = md5($password);
        $user = $this->db->get_where('user', ['username' => $username])->row_array();

        if ($user) {
            if ($pass == $user['password']) {
                $data = array(
                    "link" => $this->input->post('link'),
                    "updated_at" => date('y-m-d'),
                    "created_at" => date('y-m-d')
                );

                $this->db->insert('insta', $data);
                $this->session->set_flashdata('notif', 'created');
                redirect('insta');
            } else {
                $this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert">
                Password Anda Salah!
                </div>');

                redirect('insta');
            }
        } else {
            $this->session->set_flashdata('pesan', '<div class="alert alert-danger" role="alert">
            Auth Failed
            </div>');

            redirect('insta');
        }

        if ($this->input->post('password')) {
        }
    }

    public function updateInsta()
    {

        $id = $this->input->post('id');

        $data = array(

            "link" => $this->input->post('link'),
            "updated_at" => date('y-m-d')
        );

        $this->db->where('id', $id);
        $this->db->update('insta', $data);

        $this->session->set_flashdata('notif', 'updated');

        redirect('insta');
    }

    public function deleteInsta($id)
    {

        $this->db->where('id', $id);
        $this->db->delete('insta');
        $this->session->set_flashdata('notif', 'deleted');

        redirect('insta');
    }

    //Load CSV from spreadsheet
    public function load_csv($key = null)
    {
        if ($key != null) {

            // Set your CSV feed
            // 1IViP8QUOO6DY8fp1g2M8D1uWmMJQ0zwQIlf0SS8PX6w
            $feed = 'https://docs.google.com/spreadsheets/d/' . $key . '/export?format=csv';

            // Arrays we'll use later
            $keys = array();
            $newArray = array();

            // Function to convert CSV into associative array
            function csvToArray($file, $delimiter)
            {
                if (($handle = fopen($file, 'r')) !== FALSE) {
                    $i = 0;
                    while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) {
                        for ($j = 0; $j < count($lineArray); $j++) {
                            $arr[$i][$j] = $lineArray[$j];
                        }
                        $i++;
                    }
                    fclose($handle);
                }
                return $arr;
            }

            // Do it
            $data = csvToArray($feed, ',');

            // Set number of elements (minus 1 because we shift off the first row)
            $count = count($data) - 1;

            //Use first row for names  
            $labels = array_shift($data);

            foreach ($labels as $label) {
                $keys[] = $label;
            }

            // Add Ids, just in case we want them later
            $keys[] = 'id';

            for ($i = 0; $i < $count; $i++) {
                $data[$i][] = $i;
            }

            // Bring it all together
            for ($j = 0; $j < $count; $j++) {
                $d = array_combine($keys, $data[$j]);
                $newArray[$j] = $d;
            }

            // Print it out as JSON
            echo json_encode($newArray);
        }
    }
}
