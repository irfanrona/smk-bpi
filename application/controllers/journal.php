<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Journal extends CI_Controller
{

	public function __construct()
	{

		parent::__construct();
		$this->load->model('admin');
		$this->load->model('journal_model');
	}

	public function index($id = 0)
	{
		if ($id == 0) {

			redirect();
		}

		$data['title'] = 'SMK BPI Bandung';
		$this->journal_model->JournalIdView($id);
		$data['journal'] = $this->journal_model->getJournalId($id);

		$this->load->view('master/header', $data);
		$this->load->view('master/navbar');

		$this->load->view('journal/single_page', $data);

		$this->load->view('master/footer');
	}

	public function add()
	{
		if ($this->session->userdata('is_logged_in') != TRUE) {

			redirect("home/login");
		}

		$data['title'] = 'SMK BPI Bandung';
		$data['journals'] = $this->journal_model->getJournal();

		$this->load->view('master/header', $data);
		$this->load->view('master/navbar');

		$this->load->view('journal/add', $data);

		$this->load->view('master/footer');
	}

	public function insert()
	{
		// strip out all whitespace
		$zname_clean = preg_replace('/\s*/', '', $this->input->post('judul'));
		$zname_clean = str_replace('.', '', $zname_clean);
		// convert the string to all lowercase
		$zname_clean = strtolower($zname_clean);
		$config = array(

			'upload_path' => './upload/pdf/journal/',
			'allowed_types' => 'pdf|PDF',
			'max_size' => '1000000',
			'file_name' => $zname_clean

		);

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('file')) {
			//no upload file
		} else {
			$upload_data = $this->upload->data();
			$_POST['file_url'] = 'upload/pdf/journal/' . $zname_clean . $upload_data['file_ext'];
		}
		$this->session->set_flashdata('notif', 'ditambahkan');
		$this->journal_model->create();

		// $error = array('error' => $this->upload->display_errors());
		// var_dump($error);
		redirect('adminController/journal');
	}

	public function delete($id)
	{
		$this->journal_model->delete($id);
		$this->session->set_flashdata('notif', 'terhapus');
		redirect('adminController/journal');
	}

	public function update()
	{
		$id = $this->input->post('id');

		// strip out all whitespace
		$zname_clean = preg_replace('/\s*/', '', $this->input->post('judul'));
		$zname_clean = str_replace('.', '', $zname_clean);
		// convert the string to all lowercase
		$zname_clean = strtolower($zname_clean);
		$config = array(

			'upload_path' => './upload/pdf/journal/',
			'allowed_types' => 'pdf|PDF',
			'max_size' => '1000000',
			'file_name' => $zname_clean

		);

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('file')) {

			$pdf = $this->db->where('id', $id)->get('journal')->row();
			if ($pdf->file_url != 'upload/pdf/journal/dummy.pdf') {

				unlink('./' . $pdf->file_url);
			}

			$upload_data = $this->upload->data();
			$_POST['file_url'] = 'upload/pdf/journal/' . $upload_data['file_name'];
		} else {
		}

		$this->session->set_flashdata('notif', 'diubah');
		$this->journal_model->update($id);

		// $error = array('error' => $this->upload->display_errors());
		// var_dump($error);
		redirect('adminController/journal');
	}
}
