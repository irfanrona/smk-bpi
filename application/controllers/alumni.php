<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Alumni extends CI_Controller
{

	public function __construct()
	{

		parent::__construct();
		$this->load->model('admin');
		$this->load->model('alumni_model');
	}

	public function index()
	{

		$data['title'] = 'SMK BPI Bandung';
		$data['alumnis'] = $this->alumni_model->getAlumni(50);

		$this->load->view('master/header', $data);
		$this->load->view('master/navbar');

		$this->load->view('alumni/alumni', $data);

		$this->load->view('master/footer');
	}

	public function add()
	{
		if ($this->session->userdata('is_logged_in') != TRUE) {

			redirect("home/login");
		}

		$data['title'] = 'SMK BPI Bandung';
		$data['alumnis'] = $this->alumni_model->getAlumni();

		$this->load->view('master/header', $data);
		$this->load->view('master/navbar');

		$this->load->view('alumni/add', $data);

		$this->load->view('master/footer');
	}

	public function insert()
	{
		// strip out all whitespace
		$zname_clean = preg_replace('/\s*/', '', $this->input->post('name'));
		$zname_clean = str_replace('.', '', $zname_clean);
		// convert the string to all lowercase
		$zname_clean = strtolower($zname_clean);
		$config = array(

			'upload_path' => './upload/img/alumni/',
			'allowed_types' => 'gif|png|jpeg|jpg',
			'max_size' => '1000000',
			'file_name' => $zname_clean

		);

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('image')) {
			//no upload file
		} else {
			$upload_data = $this->upload->data();
			$_POST['image_url'] = 'upload/img/alumni/' . $zname_clean . $upload_data['file_ext'];
		}
		$this->session->set_flashdata('notif', 'ditambahkan');
		$this->alumni_model->create();
		redirect('adminController/alumni');
	}

	public function delete($id)
	{
		$this->alumni_model->delete($id);
		$this->session->set_flashdata('notif', 'terhapus');
		redirect('adminController/alumni');
	}

	public function update()
	{
		$id = $this->input->post('id');

		// strip out all whitespace
		$zname_clean = preg_replace('/\s*/', '', $this->input->post('name'));
		$zname_clean = str_replace('.', '', $zname_clean);
		// convert the string to all lowercase
		$zname_clean = strtolower($zname_clean);
		$config = array(

			'upload_path' => './upload/img/alumni/',
			'allowed_types' => 'gif|png|jpeg|jpg',
			'max_size' => '1000000',
			'file_name' => $zname_clean

		);

		$this->load->library('upload', $config);

		if ($this->upload->do_upload('image')) {

			$img = $this->db->where('id', $id)->get('alumni')->row();
			if ($img->image != 'upload/img/alumni/avatar.png') {

				unlink('./' . $img->image_url);
			}

			$upload_data = $this->upload->data();
			$_POST['image_url'] = 'upload/img/alumni/' . $upload_data['file_name'];
		} else {
		}

		$this->session->set_flashdata('notif', 'diubah');
		$this->alumni_model->update($id);
		redirect('adminController/alumni');
	}
}
