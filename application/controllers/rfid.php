<?php if (!defined('BASEPATH')) exit('No direct script access allowed');



class Rfid extends CI_Controller
{

    public function __construct()
    {

        parent::__construct();

        $this->load->model('rfid_model');
    }

    public function insert()
    {
        if (null == $this->input->get('rfid')) {
            echo "RFID is not available";
            return json_encode("RFID is not available");
        }
        $status = $this->rfid_model->insert();
        print_r($status);
        return json_encode($status);
    }

    public function show()
    {
        $data['rfids'] = $this->rfid_model->get();
        $i = 0;

        foreach ($data['rfids'] as $rfid) {
            $temp[$i] = new StdClass;

            $temp[$i]->id = $rfid->id;
            $temp[$i]->rfid = $rfid->rfid;
            $temp[$i]->name = "Nama Siswa Dummy";
            $temp[$i]->tanggal = $rfid->tanggal;
            $temp[$i]->tgl_masuk = $rfid->tgl_masuk;

            if (empty($rfid->tgl_keluar)) {
                $temp[$i]->tgl_keluar = "Belum Pulang";
                $temp[$i]->diff = "Tidak tersedia...";
            } else {
                $temp[$i]->tgl_keluar = $rfid->tgl_keluar;
                $origin = new DateTime($rfid->tgl_masuk);
                $target = new DateTime($rfid->tgl_keluar);
                $interval = $origin->diff($target);
                $temp[$i]->diff = $interval->format('%H Jam %I Menit');
            }

            $i++;
        }

        $data['rfids'] = $temp;
        $data['title'] = 'Data RFID SMK BPI Bandung';

        $this->load->view('master/header', $data);

        $this->load->view('master/navbar');

        $this->load->view('rfid');

        $this->load->view('master/footer');
    }
}
