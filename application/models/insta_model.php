<?php
class insta_model extends CI_model
{
    function getInsta($limit = 20)
    {
        $this->db->order_by('id', 'DESC');
        $this->db->limit($limit);
        return $this->db->get('insta')->result();
    }
    
}
?>