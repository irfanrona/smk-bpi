<?php
class alumni_model extends CI_model
{
    function getAlumni($limit = 20)
    {
        $this->db->order_by('id', 'DESC');
        $this->db->limit($limit);
        return $this->db->get('alumni')->result();
    }

    function getShowedAlumni($limit = 3)
    {
        $this->db->where('show', 'true');
        $this->db->order_by('id', 'DESC');
        $this->db->limit($limit);
        return $this->db->get('alumni')->result();
    }

    public function create()
    {
        if (!$_POST['image_url']) {
            $img = 'upload/img/alumni/avatar.png';
        } else {
            $img = $_POST['image_url'];
        }
        $data = array(

            "name" => $this->input->post('name'),
            "subtitle" => $this->input->post('subtitle'),
            "description" => $this->input->post('description'),
            "image_url" => $img,
            "show" => $this->input->post('show'),
            'upload_date'  => date('Y-m-d H:i:s'),
            'modified_date'     => date('Y-m-d H:i:s')
        );

        $this->db->insert('alumni', $data);
    }

    public function update($id)
    {
        if (!$_POST['image_url']) {
            $data = array(
                "name" => $this->input->post('name'),
                "subtitle" => $this->input->post('subtitle'),
                "description" => $this->input->post('description'),
                "show" => $this->input->post('show'),
                'modified_date'     => date('Y-m-d H:i:s')
            );
        } else {
            $img = $_POST['image_url'];

            $data = array(
                "name" => $this->input->post('name'),
                "subtitle" => $this->input->post('subtitle'),
                "description" => $this->input->post('description'),
                "image_url" => $img,
                "show" => $this->input->post('show'),
                'modified_date'     => date('Y-m-d H:i:s')
            );
        }
        $this->db->where('id', $id);
        $this->db->update('alumni', $data);
    }

    public function delete($id)
    {
        $this->db->delete('alumni', array('id' => $id));
    }
}
