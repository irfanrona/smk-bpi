<?php
class journal_model extends CI_model
{
    function getJournal($limit = 20)
    {
        $this->db->order_by('id', 'DESC');
        $this->db->limit($limit);
        return $this->db->get('journal')->result();
    }

    function getShowedJournal($limit = 3)
    {
        $this->db->where('show', 'true');
        $this->db->order_by('id', 'DESC');
        $this->db->limit($limit);
        return $this->db->get('journal')->result();
    }

    public function getJournalId($id)
    {
        return $this->db->where('id', $id)->get('journal')->row();
    }

    public function create()
    {
        if (!$_POST['file_url']) {
            $pdf = 'upload/pdf/journal/dummy.pdf';
        } else {
            $pdf = $_POST['file_url'];
        }
        $data = array(

            "judul" => $this->input->post('judul'),
            "penulis" => $this->input->post('penulis'),
            "abstrak" => $this->input->post('abstrak', false),
            "file_url" => $pdf,
            "show" => $this->input->post('show'),
            'upload_date'  => date('Y-m-d H:i:s'),
            'modified_date'     => date('Y-m-d H:i:s')
        );

        $this->db->insert('journal', $data);
    }

    public function update($id)
    {
        if (!$_POST['file_url']) {
            $data = array(
                "judul" => $this->input->post('judul'),
                "penulis" => $this->input->post('penulis'),
                "abstrak" => $this->input->post('abstrak'),
                "show" => $this->input->post('show'),
                'modified_date'     => date('Y-m-d H:i:s')
            );
        } else {
            $pdf = $_POST['file_url'];

            $data = array(
                "judul" => $this->input->post('judul'),
                "penulis" => $this->input->post('penulis'),
                "abstrak" => $this->input->post('abstrak'),
                "file_url" => $pdf,
                "show" => $this->input->post('show'),
                'modified_date'     => date('Y-m-d H:i:s')
            );
        }
        $this->db->where('id', $id);
        $this->db->update('journal', $data);
    }

    public function JournalIdView($id)
    {
        $journal = $this->getJournalId($id);

        $data = array(
            "count" => $journal->count + 1
        );
        $this->db->where('id', $id);
        $this->db->update('journal', $data);
    }

    public function delete($id)
    {
        $this->db->delete('journal', array('id' => $id));
    }
}
