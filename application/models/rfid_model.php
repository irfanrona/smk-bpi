<?php
class Rfid_model extends CI_model
{
    public $rfid;
    public $tanggal;
    public $tgl_masuk;
    public $tgl_keluar;
    public $device_id;

    function get($limit = 20)
    {
        $this->db->order_by('id', 'DESC');
        $this->db->limit($limit);
        return $this->db->get('rfid')->result();
    }

    function insert()
    {
        $present = $this->is_present($this->input->get('rfid'), date('Y-m-d'));

        if (isset($present[0]->rfid)) {

            $this->rfid = $present[0]->rfid;
            $this->tanggal = $present[0]->tanggal;
            $this->tgl_masuk = $present[0]->tgl_masuk;
            $this->tgl_keluar = date('Y-m-d H:i:s');
            $this->device_id = $this->input->get('device_id');

            $this->db->update('rfid', $this, array('id' => $present[0]->id));
            return "Data Pulang";
        } else {
            $this->rfid = $this->input->get('rfid');
            $this->tanggal = date('Y-m-d');
            $this->tgl_masuk = date('Y-m-d H:i:s');
            $this->tgl_keluar = null;
            $this->device_id = $this->input->get('device_id');

            $this->db->insert('rfid', $this);
            return "Data Masuk";
        }
    }

    public function is_present($rfid = null, $tanggal = null)
    {
        $query = $this->db->get_where('rfid', array('rfid' => $rfid, 'tanggal' => $tanggal), 1);
        return $query->result();
    }
}
