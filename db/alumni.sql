/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 100413
 Source Host           : localhost:3306
 Source Schema         : db_smk_main

 Target Server Type    : MySQL
 Target Server Version : 100413
 File Encoding         : 65001

 Date: 25/03/2022 19:19:36
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for alumni
-- ----------------------------
DROP TABLE IF EXISTS `alumni`;
CREATE TABLE `alumni`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `subtitle` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `image_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `upload_date` datetime(0) NULL DEFAULT NULL,
  `modified_date` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `show` enum('true','false') CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'false',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 34 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of alumni
-- ----------------------------
INSERT INTO `alumni` VALUES (1, 'Hamdan Hanafi', 'PT AccelByte Teknologi Indonesia', 'SMK BPI adalah sekolah yang menyenangkan, Alhamdulillah, saya dibimbing dan dididik oleh guru-guru yang berpengalaman serta selalu disiplin penuh terhadap siswanya.', 'upload/img/alumni/hamdanhanafi.jpeg', NULL, '2022-03-25 12:29:32', 'false');
INSERT INTO `alumni` VALUES (2, 'Achmad Try August', 'PT. Indostorage Solusi Technologi', 'Luar biasa, karena saya sangat menikmati setiap detik saat bersekolah di SMK BPI. Banyak hal dan kenangan menarik yang saya bisa dapat di SMK BPI', 'upload/img/alumni/achmadtryaugust.jpg', NULL, '2022-03-25 13:07:13', 'false');
INSERT INTO `alumni` VALUES (3, 'Meta Rahayu', 'Manajemen UPI', 'Alhamdulillah, banyak ilmu dan manfaat yang saya dapatkan selama belajar di SMK BPI, berkat program yang dilakukan di sekolah, saya dapat menjadi pribadi yang lebih baik', 'upload/img/alumni/meta.jpg', NULL, '2022-03-25 15:29:53', 'false');
INSERT INTO `alumni` VALUES (4, 'Anastasya Reskianissa', 'Pendidikan Ilmu Komputer UPI', 'SMK BPI adalah tempat belajar yang sangat berkesan. Tidak hanya akademik, saya belajar bagaimana menjadi sosok pribadi yang ideal sesuai motto; bermartabat, berkualitas, dan terpercaya. Dari totalitas tenaga pendidik dalam membimbing para siswa, saya sela', 'upload/img/alumni/anastasya.jpeg', NULL, '2022-03-25 17:51:54', 'true');
INSERT INTO `alumni` VALUES (5, 'Alif gufron pratama', 'PT. Melvar Lintasnusa', 'Saya bangga menjadi lulusan SMK BPI, Selama belajar di SMK BPI saya mendapat banyak sekali pelajaran yang bermanfaat dari Ibu/Bapa Guru baik Di bidang akademik dan non akademik begitu bangga menjadi lulusan SMK BPI lulus sekolah siap kerja dan siap kuliah', 'upload/img/alumni/alif.jpeg', NULL, '2022-03-25 17:51:46', 'true');
INSERT INTO `alumni` VALUES (6, 'Diva Widya Pangesti', 'PT KB BUKOPIN,Tbk', 'Sangat senang rasanya sudah menjadi siswa di SMK BPI Bandung, banyak sekali ilmu yang terpakai setelah saya bekerja, mendapatkan banyak teman, kagum kepada guru guru yang sangat baik dan ramah serta bersedia mengajarkan muridnya sampai pandai, terima kasi', 'upload/img/alumni/diva.jpeg', NULL, '2022-03-25 16:26:15', 'true');
INSERT INTO `alumni` VALUES (21, 'Ghanil Fuady Sumpena, S.Tr.Sn.', 'PT Unikom Kreasi Media', 'Alumni ini belum memasukan quote, jika kamu adalah alumni atau kenal dengan alumni ini, segera hubungi kampus SMK BPI.', 'upload/img/alumni/ghanilfuady.jpeg', '2022-03-25 12:28:42', '2022-03-25 12:30:49', 'false');
INSERT INTO `alumni` VALUES (20, 'Raviani', 'PT. Agra Nara Indonesia', 'Alumni ini belum memasukan quote, jika kamu adalah alumni atau kenal dengan alumni ini, segera hubungi kampus SMK BPI', 'upload/img/alumni/raviani.jpeg', '2022-03-25 12:27:50', '2022-03-25 12:27:50', 'false');
INSERT INTO `alumni` VALUES (19, 'M. Persada Dwinanda', 'dotERB Solution', 'Alumni ini belum memasukan quote, jika kamu adalah alumni atau kenal dengan alumni ini, segera hubungi kampus SMK BPI', 'upload/img/alumni/mpersadadwinanda.jpeg', '2022-03-25 11:41:45', '2022-03-25 12:00:02', 'false');
INSERT INTO `alumni` VALUES (22, 'Dian Andria', 'Senopati Indra Teknologi', 'Alumni ini belum memasukan quote, jika kamu adalah alumni atau kenal dengan alumni ini, segera hubungi kampus SMK BPI.', 'upload/img/alumni/dianandria.jpeg', '2022-03-25 12:30:05', '2022-03-25 12:30:05', 'false');
INSERT INTO `alumni` VALUES (23, 'Yoga Tri', 'PT Arkamaya', 'Alumni ini belum memasukan quote, jika kamu adalah alumni atau kenal dengan alumni ini, segera hubungi kampus SMK BPI.', 'upload/img/alumni/yogatri.jpeg', '2022-03-25 12:30:30', '2022-03-25 12:30:30', 'false');
INSERT INTO `alumni` VALUES (24, 'Yesica Anggraeni', 'APTAVIS', 'Alumni ini belum memasukan quote, jika kamu adalah alumni atau kenal dengan alumni ini, segera hubungi kampus SMK BPI.', 'upload/img/alumni/yesicaanggraeni.jpg', '2022-03-25 12:32:47', '2022-03-25 12:32:47', 'false');
INSERT INTO `alumni` VALUES (25, 'Willa Aprilia', 'PT. XL Axiata', 'Alumni ini belum memasukan quote, jika kamu adalah alumni atau kenal dengan alumni ini, segera hubungi kampus SMK BPI.', 'upload/img/alumni/willaaprilia.jpg', '2022-03-25 12:59:52', '2022-03-25 13:10:25', 'false');
INSERT INTO `alumni` VALUES (26, 'Muhamad Syawal Radiansyah ', 'Dinas Kesehatan Jawa Barat', 'Alumni ini belum memasukan quote, jika kamu adalah alumni atau kenal dengan alumni ini, segera hubungi kampus SMK BPI.', 'upload/img/alumni/muhamadsyawalradiansyah.jpg', '2022-03-25 13:00:43', '2022-03-25 13:10:13', 'false');
INSERT INTO `alumni` VALUES (27, 'Rivan Arivian', 'PT. Citra Jelajah Informatika', 'Alumni ini belum memasukan quote, jika kamu adalah alumni atau kenal dengan alumni ini, segera hubungi kampus SMK BPI.', 'upload/img/alumni/rivanarivian.jpg', '2022-03-25 13:01:24', '2022-03-25 13:01:24', 'false');
INSERT INTO `alumni` VALUES (28, 'Muhamad Zhentra Nurfauzy', 'Dinas Informasi dan Pengolah Data TNI AD', 'Alumni ini belum memasukan quote, jika kamu adalah alumni atau kenal dengan alumni ini, segera hubungi kampus SMK BPI.', 'upload/img/alumni/muhamadzhentranurfauzy.jpg', '2022-03-25 13:03:03', '2022-03-25 13:03:03', 'false');
INSERT INTO `alumni` VALUES (29, 'Hasby Shahid Ash Siddiq', 'PT Inet Global Indo', 'Alumni ini belum memasukan quote, jika kamu adalah alumni atau kenal dengan alumni ini, segera hubungi kampus SMK BPI.', 'upload/img/alumni/hasbyshahidashsiddiq.jpg', '2022-03-25 13:03:40', '2022-03-25 13:03:40', 'false');
INSERT INTO `alumni` VALUES (30, 'Ariq Esa Pratama', 'Broder Service Laptop', 'Alumni ini belum memasukan quote, jika kamu adalah alumni atau kenal dengan alumni ini, segera hubungi kampus SMK BPI.', 'upload/img/alumni/ariqesapratama.jpg', '2022-03-25 13:04:23', '2022-03-25 13:04:23', 'false');
INSERT INTO `alumni` VALUES (31, 'Abdi', 'PT Solvindo Prima Conexio', 'Alumni ini belum memasukan quote, jika kamu adalah alumni atau kenal dengan alumni ini, segera hubungi kampus SMK BPI.', 'upload/img/alumni/abdi.jpg', '2022-03-25 13:04:55', '2022-03-25 13:04:55', 'false');
INSERT INTO `alumni` VALUES (32, 'Wirandhika', 'PT. Melvar Lintasnusa', 'Alumni ini belum memasukan quote, jika kamu adalah alumni atau kenal dengan alumni ini, segera hubungi kampus SMK BPI.', 'upload/img/alumni/wirandhika.jpg', '2022-03-25 13:05:50', '2022-03-25 13:08:09', 'false');
INSERT INTO `alumni` VALUES (33, 'Rakash Zulfikar', 'Lintasarta', 'Alumni ini belum memasukan quote, jika kamu adalah alumni atau kenal dengan alumni ini, segera hubungi kampus SMK BPI.', 'upload/img/alumni/rakashzulfikar.jpg', '2022-03-25 13:06:41', '2022-03-25 13:08:53', 'false');

SET FOREIGN_KEY_CHECKS = 1;
