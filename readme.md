[![Contributors][contributors-shield]][contributors-url]
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/othneildrew/Best-README-Template">
    <img src="assets/images/logo.png" alt="Logo" width="100" height="80">
  </a>

  <h3 align="center">Website SMK BPI</h3>

  <p align="center">
    Clean code website existing SMK BPI, untuk tujuan pengembangan!
    <br />
    <a href="https://github.com/irfanrona/smk-bpi"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://smkbpi.sch.id">View Site</a>
    ·
    <a href="https://github.com/irfanrona/smk-bpi/issues">Report Bug</a>
    ·
    <a href="https://github.com/irfanrona/smk-bpi/issues">Request Feature</a>
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#installation">Instalation</a>
    </li>
    <li>
      <a href="#new-feature">New Feature</a>
      <ul>
        <li><a href="#instagram-feed">Instagram Feed</a></li>
        <li><a href="#rfid">RFID</a></li>
      </ul>
    </li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>

## Installation

_Dokumentasi instalasi akan langsung melompat pada instalasi aplikasi, maka dari itu untuk prerequesite nya, pastikan sudah menginstal prerequesite berikut ini:_

- Apache, Mysql, PHP 5.4+ (atau langsung install XAMPP)

_Proses Instalasi:_

1. Clone the repo
   ```sh
   git clone https://github.com/irfanrona/smk-bpi.git
   ```
2. Sunting config database sesuaikan dengan lokal mesin anda `application/config/database.php`
   ```php
   $db['default']['hostname'] = 'localhost';
   $db['default']['username'] = 'root';
   $db['default']['password'] = '';
   $db['default']['database'] = 'db_anda';
   ```
3. Export schema sql pada database di folder `db\smkbisch_update.sql`
4. akses credential akun login silakan hubungi [Developer](https://linkedin.com/in/irfanrona)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- Known Bugs -->

## Bugs

Feature edit journal not fixed yet! CORS Problem!

<!-- New Feature -->

## New Feature

Berikut ini fitur-fitur dari SMK yang terbaru dan sedang dalam tahap pengembangan dan pemantauan.
Cara penggunaan fitur juga termasuk pada dokumen ini.

<!-- Journal -->

### Journal

- Manajemen Feed Journal pendidik
  ```
  localhost/smk-bpi/adminController/journal
  ```

<!-- Instagram -->

### Instagram

- Manajemen Feed Instagram url
  ```
  localhost/smk-bpi/insta
  ```

<!-- RFID -->

### RFID

- Cara membaca seluruh data RFID
  ```
  localhost/smk-bpi/rfid/show
  ```
- Cara melakukan presensi menggunakan RFID
  url `yourdomain.com/rfid/insert`
  parameter
  - **Mandatory** `rfid`
  - _Optional_ `device_id`
    contoh penggunaan
  ```
  localhost/smk-bpi/rfid/insert?rfid=12345678&device_id=1
  localhost/smk-bpi/rfid/insert?rfid=23956235
  ```

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- LICENSE -->

## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- CONTACT -->

## Contact

Irfan Rona - [@irfanrona](https://linkedin.com/in/irfanrona) - irfan.rona95@student.upi.edu
Next Developer - [@your_social](https://twitter.com/your_username) - email@example.com

Project Link: [https://gitlab.com/irfanrona/smk-bpi](https://gitlab.com/irfanrona/smk-bpi)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->

[contributors-shield]: https://img.shields.io/gitlab/contributors/irfanrona/smk-bpi.svg?style=for-the-badge
[contributors-url]: https://gitlab.com/irfanrona/smk-bpi/graphs/contributors
[forks-shield]: https://img.shields.io/gitlab/forks/irfanrona/smk-bpi.svg?style=for-the-badge
[forks-url]: https://gitlab.com/irfanrona/smk-bpi/network/members
[issues-shield]: https://img.shields.io/gitlab/issues/irfanrona/smk-bpi.svg?style=for-the-badge
[issues-url]: https://gitlab.com/irfanrona/smk-bpi/issues
[license-shield]: https://img.shields.io/gitlab/license/irfanrona/smk-bpi.svg?style=for-the-badge
[license-url]: https://gitlab.com/irfanrona/smk-bpi/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://linkedin.com/in/irfanrona
